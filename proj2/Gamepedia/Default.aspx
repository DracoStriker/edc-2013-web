﻿<%@ Page Title="Gamepedia" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Gamepedia._Default" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2>the free encyclopedia that anyone can edit.</h2>
            </hgroup>
            <p>
                Explore the vast gaming world.
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <h3>Games:</h3>
    <asp:DropDownList ID="dropdownlist" runat="server" />
    <asp:Button ID="Sort" runat="server" OnClick="Sort_Games" Text="Sort" />
    <table>
        <asp:Repeater ID="repeater" runat="server">
            <HeaderTemplate>
                <tr>
                    <th>Name</th>
                    <th>Developer</th>
                    <th>Genres</th>
                    <th>Date</th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <a href="Display.aspx?game=<%#XPath("@name")%>">
                            <%#XPath("@name")%>
                        </a>
                    </td>
                    <td>
                        <%#XPath("@developers").ToString().Replace("\\n","<br/>")%>
                    </td>
                    <td>
                        <%#XPath("@genres").ToString().Replace("\\n","<br/>")%>
                    </td>
                    <td>
                        <%#XPath("@date")%>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Content>
