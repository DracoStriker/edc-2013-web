﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Gamepedia
{
    public partial class Edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                XmlDocument xdoc = XmlDataSource1.GetXmlDocument();
                String gameName = Request.QueryString["game"];
                XmlElement games = null;
                XmlNode game = null;

                if (gameName != "")
                {

                    games = xdoc.SelectSingleNode("/games[1]") as XmlElement;


                    game = xdoc.SelectSingleNode("games/game[@name= '" + gameName + "' ]") as XmlElement;


                    /* Verifica se ja existe algum "games"
                     *
                     * Para o caso de nao existir, cria um novo
                     * 
                     */
                    if (games == null)
                    {
                        games = xdoc.CreateElement("games");
                        xdoc.AppendChild(games);
                    }

                    /* Caso esteja a editar um jogo, coloca os campos das textboxes com os valores dos campos do jogo em questao*/
                    if (game != null)
                    {
                        /*Name*/
                        name.Text = gameName;

                        /*Developer*/
                        try
                        {
                            developers.Text = game.Attributes["developers"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            developers.Text = "";
                        }

                        /*Distributor*/
                        try
                        {
                            distributors.Text = game.Attributes["distributors"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            distributors.Text = "";
                        }

                        /*Director*/
                        try
                        {
                            directors.Text = game.Attributes["directors"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            directors.Text = "";
                        }


                        /*Producer*/
                        try
                        {
                            producers.Text = game.Attributes["producers"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            producers.Text = "";
                        }

                        /*Designer*/
                        try
                        {
                            designers.Text = game.Attributes["designers"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            designers.Text = "";
                        }


                        /*Artist*/
                        try
                        {
                            artists.Text = game.Attributes["artists"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            artists.Text = "";
                        }



                        /*Writer*/
                        try
                        {
                            writers.Text = game.Attributes["writers"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            writers.Text = "";
                        }


                        /*Composer*/
                        try
                        {
                            composers.Text = game.Attributes["composers"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            composers.Text = "";
                        }


                        /*Series*/
                        try
                        {
                            series.Text = game.Attributes["series"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            series.Text = "";
                        }


                        /*Platform*/
                        try
                        {
                            platforms.Text = game.Attributes["platforms"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            platforms.Text = "";
                        }


                        /*Date*/
                        try
                        {
                            date.Text = game.Attributes["dates"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            date.Text = "";
                        }

                        /*Genre*/
                        try
                        {
                            genres.Text = game.Attributes["genres"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            genres.Text = "";
                        }


                        /*Modes*/
                        try
                        {
                            modes.Text = game.Attributes["modes"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            modes.Text = "";
                        }


                        /*Description*/
                        try
                        {
                            description.Text = game.Attributes["description"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            description.Text = "";
                        }

                    }
                }
                /* Caso contrario coloca os textboxes sem texto nenhum*/
                else
                {
                    /*Name*/
                    name.Text = "";

                    /*Developers*/
                    developers.Text = "";

                    /*Distributors*/
                    distributors.Text = "";

                    /*Directors*/
                    directors.Text = "";

                    /*Producers*/
                    producers.Text = "";

                    /*Designers*/
                    designers.Text = "";

                    /*Artists*/
                    artists.Text = "";

                    /*Writers*/
                    writers.Text = "";

                    /*Composers*/
                    composers.Text = "";

                    /*Sries*/
                    series.Text = "";

                    /*Platforms*/
                    platforms.Text = "";

                    /*Date*/
                    date.Text = "";

                    /*Genres*/
                    genres.Text = "";

                    /*Modes*/
                    modes.Text = "";

                    /*Description*/
                    description.Text = "";
                }
            }
        }

        protected void Edit_Game(object sender, EventArgs e)
        {
            XmlDocument xdoc = XmlDataSource1.GetXmlDocument();
            XmlElement games = xdoc.SelectSingleNode("/games[1]") as XmlElement;
            XmlElement game = null;
            String gameName = Request.QueryString["game"];
            game = xdoc.SelectSingleNode("games/game[@name='" + gameName + "']") as XmlElement;
            if (game != null)
            {
                xdoc.DocumentElement.RemoveChild(game);
            }
            addGame(games, null);

            Response.Redirect("~/Default.aspx");
        }

        private void addGame(XmlElement games, XmlElement game)
        {
            XmlDocument xdoc = XmlDataSource1.GetXmlDocument();

            /*cria o elemento game onde sera armazenada a respectiva info*/
            game = xdoc.CreateElement("game");
            //games.AppendChild(game);

            /*Name*/
            XmlAttribute nome = xdoc.CreateAttribute("name");
            nome.Value = name.Text;

            /*Developers*/
            XmlAttribute developer = xdoc.CreateAttribute("developers");
            developer.Value = developers.Text;

            /*Distributors*/
            XmlAttribute distributor = xdoc.CreateAttribute("distributors");
            distributor.Value = distributors.Text;

            /*Directors*/
            XmlAttribute director = xdoc.CreateAttribute("directors");
            director.Value = directors.Text;

            /*Producers*/
            XmlAttribute producer = xdoc.CreateAttribute("producers");
            producer.Value = producers.Text;

            /*Designers*/
            XmlAttribute designer = xdoc.CreateAttribute("designers");
            designer.Value = designers.Text;

            /*Artists*/
            XmlAttribute artist = xdoc.CreateAttribute("artists");
            artist.Value = artists.Text;

            /*Writers*/
            XmlAttribute writer = xdoc.CreateAttribute("writers");
            writer.Value = writers.Text;

            /*Composers*/
            XmlAttribute composer = xdoc.CreateAttribute("composers");
            composer.Value = composers.Text;

            /*Series*/
            XmlAttribute serie = xdoc.CreateAttribute("series");
            serie.Value = series.Text;

            /*Platforms*/
            XmlAttribute platform = xdoc.CreateAttribute("platforms");
            platform.Value = platforms.Text;

            /*Date*/
            XmlAttribute date1 = xdoc.CreateAttribute("date");
            date1.Value = date.Text;

            /*Genres*/
            XmlAttribute genre = xdoc.CreateAttribute("genres");
            genre.Value = genres.Text;

            /*Modes*/
            XmlAttribute mode = xdoc.CreateAttribute("modes");
            mode.Value = modes.Text;

            /*Publishers*/
            XmlAttribute publisher = xdoc.CreateAttribute("publishers");
            publisher.Value = publishers.Text;

            /*Description*/
            XmlAttribute description1 = xdoc.CreateAttribute("description");
            description1.Value = description.Text;

            game.Attributes.Append(nome);
            game.Attributes.Append(developer);
            game.Attributes.Append(distributor);
            game.Attributes.Append(publisher);
            game.Attributes.Append(director);
            game.Attributes.Append(producer);
            game.Attributes.Append(designer);
            game.Attributes.Append(artist);
            game.Attributes.Append(writer);
            game.Attributes.Append(composer);
            game.Attributes.Append(serie);
            game.Attributes.Append(platform);
            game.Attributes.Append(date1);
            game.Attributes.Append(genre);
            game.Attributes.Append(mode);
            game.Attributes.Append(description1);
            xdoc.DocumentElement.AppendChild(game);

            XmlDataSource1.Save();
        }
    }
}