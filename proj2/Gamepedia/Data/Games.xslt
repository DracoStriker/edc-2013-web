﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- Display all games in the XML database. -->
  <xsl:param name="sort">genres</xsl:param>
  <xsl:template match="/">
    <games>
      <xsl:apply-templates select="games/game">
        <xsl:sort select="@*[name()=$sort]" order="ascending"/>
      </xsl:apply-templates>
    </games>
  </xsl:template>
  <!-- Display a game in the XML database. -->
  <xsl:template match="game">
    <game>
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

      <xsl:attribute name="developers">
        <xsl:value-of select="@developers"/>
      </xsl:attribute>

      <xsl:attribute name="genres">
        <xsl:value-of select="@genres"/>
      </xsl:attribute>

      <xsl:attribute name="date">
        <xsl:value-of select="@date"/>
      </xsl:attribute>
    </game>
  </xsl:template>
</xsl:stylesheet>