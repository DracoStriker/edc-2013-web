﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- Select the pretended game to display it's the full data in the XML database. -->
  <xsl:template match="/">
    <xsl:param name="name"/>
    <games>
      <xsl:apply-templates select="games/game[@name=$name]"/>
    </games>
  </xsl:template>
  <!-- Display the full data of a Game in the XML database. -->
  <xsl:template match="game">
    <game>
      
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>

      <xsl:attribute name="developers">
        <xsl:value-of select="@developers"/>
      </xsl:attribute>

      <xsl:attribute name="distributors">
        <xsl:value-of select="@distributors"/>
      </xsl:attribute>

      <xsl:attribute name="publishers">
        <xsl:value-of select="@publishers"/>
      </xsl:attribute>

      <xsl:attribute name="directors">
        <xsl:value-of select="@directors"/>
      </xsl:attribute>
      
      <xsl:attribute name="producers">
        <xsl:value-of select="@producers"/>
      </xsl:attribute>

      <xsl:attribute name="designers">
        <xsl:value-of select="@designers"/>
      </xsl:attribute>

      <xsl:attribute name="artists">
        <xsl:value-of select="@artists"/>
      </xsl:attribute>

      <xsl:attribute name="writers">
        <xsl:value-of select="@writers"/>
      </xsl:attribute>

      <xsl:attribute name="composers">
        <xsl:value-of select="@composers"/>
      </xsl:attribute>

      <xsl:attribute name="series">
        <xsl:value-of select="@series"/>
      </xsl:attribute>

      <xsl:attribute name="platforms">
        <xsl:value-of select="@platforms"/>
      </xsl:attribute>

      <xsl:attribute name="date">
        <xsl:value-of select="@date"/>
      </xsl:attribute>

      <xsl:attribute name="genres">
        <xsl:value-of select="@genres"/>
      </xsl:attribute>
      
      <xsl:attribute name="modes">
        <xsl:value-of select="@modes"/>
      </xsl:attribute>

      <xsl:attribute name="description">
        <xsl:value-of select="@description"/>
      </xsl:attribute>
      
    </game>
  </xsl:template>
</xsl:stylesheet>