﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Xsl;

namespace Gamepedia
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                XmlDataSource xmlDocument = new XmlDataSource();
                xmlDocument.DataFile = "~/Data/Games.xml";
                xmlDocument.TransformFile = "~/Data/Games.xslt";
                repeater.DataSource = xmlDocument;
                repeater.DataBind();
                dropdownlist.Items.Add("name");
                dropdownlist.Items.Add("developers");
                dropdownlist.Items.Add("genres");
                dropdownlist.Items.Add("date");
            }
        }

        protected void Sort_Games(object sender, EventArgs e)
        {
            XmlDataSource xmlDocument = new XmlDataSource();
            xmlDocument.DataFile = "~/Data/Games.xml";
            xmlDocument.TransformFile = "~/Data/Games.xslt";
            xmlDocument.EnableCaching = false;
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("sort", "", dropdownlist.SelectedItem.Value);
            xmlDocument.TransformArgumentList = args;
            repeater.DataSource = xmlDocument;
            repeater.DataBind();
        }
    }
}