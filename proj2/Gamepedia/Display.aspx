﻿<%@ Page Title="Game Info" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Display.aspx.cs" Inherits="Gamepedia.Display" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <hgroup class="title">
        <h1><%: Title %>.</h1>
    </hgroup>
    <p>
        Here you can check the detailed information about the game.
    </p>
    <fieldset>
        <legend>Edit Game Form</legend>
        <ol>
            <li>
                <b><asp:Label ID="Label1" runat="server">Name:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label ID="name" runat="server"/>
            </li>
            <li>	
                <b><asp:Label ID="Label2" runat="server">Developers:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label ID="developers" runat="server"/>
            </li>	
            <li>	
                <b><asp:Label ID="Label3" runat="server">Distributors:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="distributors"/>
            </li>
            <li>
                <b><asp:Label ID="Label4" runat="server">Directors:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="directors"/>
            </li>
            <li>
                <b><asp:Label ID="Label5" runat="server">Publishers:</asp:Label></b>	
            </li>	
            <li>	
                <asp:Label runat="server" ID="publishers"/>
            </li>
            <li>	
                <b><asp:Label ID="Label6" runat="server">Producers:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="producers" />
            </li>
            <li>	
                <b><asp:Label ID="Label7" runat="server">Designers:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="designers"/>
            </li>
            <li>	
                <b><asp:Label ID="Label8" runat="server">Artists:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="artists"/>
            </li>
            <li>	
                <b><asp:Label ID="Label9" runat="server">Writers:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="writers"/>
            </li>
            <li>	
                <b><asp:Label ID="Label10" runat="server">Composers:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="composers" />
            </li>
            <li>	
                <b><asp:Label ID="Label11" runat="server">Series:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="series" />
            </li>
            <li>	
                <b><asp:Label ID="Label12" runat="server">Platforms:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="platforms"/>
            </li>
            <li>	
                <b><asp:Label ID="Label13" runat="server">Date:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="date"/>
            </li>
            <li>	
                <b><asp:Label ID="Label14" runat="server">Genres:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="genres"/>
            </li>
            <li>	
                <b><asp:Label ID="Label15" runat="server">Modes:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="modes"/>
            </li>
            <li>	
                <b><asp:Label ID="Label16" runat="server">Description:</asp:Label></b>
            </li>	
            <li>	
                <asp:Label runat="server" ID="description"/>
            </li>
        </ol>
        <asp:Button ID="EditGame" runat="server" OnClick="Edit_Game" Text="Edit Game" />
        <asp:Button ID="RemoveGame" runat="server" OnClick="Remove_Game" Text="Remove Game" />
    </fieldset>
    <asp:XmlDataSource ID="XmlDataSource" runat="server" DataFile="~/Data/Games.xml" EnableCaching="False"/>
</asp:Content>
