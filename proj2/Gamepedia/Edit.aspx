﻿<%@ Page Title="Edit Game" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Gamepedia.Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
    </hgroup>
    <p>
        Here you can edit the information about the game.
    </p>
    <fieldset>
        <legend>Edit Game Form</legend>
        <ol>
            <li>
                <asp:Label ID="Label1" runat="server">Name:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="name" />
            </li>
            <li>
                <asp:Label ID="Label2" runat="server">Developers:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="developers" />
            </li>
            <li>
                <asp:Label ID="Label3" runat="server">Distributors:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="distributors" />
            </li>
            <li>
                <asp:Label ID="Label4" runat="server">Directors:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="directors" />
            </li>
            <li>
                <asp:Label ID="Label5" runat="server">Publishers:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="publishers" />
            </li>
            <li>
                <asp:Label ID="Label6" runat="server">Producers:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="producers" />
            </li>
            <li>
                <asp:Label ID="Label7" runat="server">Designers:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="designers" />
            </li>
            <li>
                <asp:Label ID="Label8" runat="server">Artists:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="artists" />
            </li>
            <li>
                <asp:Label ID="Label9" runat="server">Writers:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="writers" />
            </li>
            <li>
                <asp:Label ID="Label10" runat="server">Composers:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="composers" />
            </li>
            <li>
                <asp:Label ID="Label11" runat="server">Series:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="series" />
            </li>
            <li>
                <asp:Label ID="Label12" runat="server">Platforms:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="platforms" />
            </li>
            <li>
                <asp:Label ID="Label13" runat="server">Date:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="date" TextMode="Date" />
            </li>
            <li>
                <asp:Label ID="Label14" runat="server">Genres:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="genres" />
            </li>
            <li>
                <asp:Label ID="Label15" runat="server">Modes:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="modes" />
            </li>
            <li>
                <asp:Label ID="Label16" runat="server">Description:</asp:Label>
            </li>
            <li>
                <asp:TextBox runat="server" ID="description" />
            </li>
        </ol>
        <asp:Button ID="GameEditButton" runat="server" OnClick="Edit_Game" Text="Submit" />
    </fieldset>
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/Data/Games.xml" EnableCaching="False" />
</asp:Content>
