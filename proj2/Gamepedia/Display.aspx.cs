﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using System.IO;

namespace Gamepedia
{
    public partial class Display : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String gameName = Request.QueryString["game"];

            XmlDocument xdoc = XmlDataSource.GetXmlDocument();

            XmlNode game = xdoc.SelectSingleNode("games/game[@name= '" + gameName + "' ]") as XmlElement;

            if (game != null)
            {
                /*Name*/
                name.Text = gameName;

                /*Developer*/
                try
                {
                    developers.Text = game.Attributes["developers"].Value.ToString().Replace("\\n","<br/>");
                }
                catch (Exception ex)
                {
                    developers.Text = "";
                }

                /*Distributor*/
                try
                {
                    distributors.Text = game.Attributes["distributors"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    distributors.Text = "";
                }

                /*Director*/
                try
                {
                    directors.Text = game.Attributes["directors"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    directors.Text = "";
                }


                /*Producer*/
                try
                {
                    producers.Text = game.Attributes["producers"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    producers.Text = "";
                }

                /*Designer*/
                try
                {
                    designers.Text = game.Attributes["designers"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    designers.Text = "";
                }


                /*Artist*/
                try
                {
                    artists.Text = game.Attributes["artists"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    artists.Text = "";
                }



                /*Writer*/
                try
                {
                    writers.Text = game.Attributes["writers"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    writers.Text = "";
                }


                /*Composer*/
                try
                {
                    composers.Text = game.Attributes["composers"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    composers.Text = "";
                }


                /*Series*/
                try
                {
                    series.Text = game.Attributes["series"].Value.ToString();
                }
                catch (Exception ex)
                {
                    series.Text = "";
                }


                /*Platform*/
                try
                {
                    platforms.Text = game.Attributes["platforms"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    platforms.Text = "";
                }


                /*Date*/
                try
                {
                    date.Text = game.Attributes["dates"].Value.ToString();
                }
                catch (Exception ex)
                {
                    date.Text = "";
                }

                /*Genre*/
                try
                {
                    genres.Text = game.Attributes["genres"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    genres.Text = "";
                }


                /*Modes*/
                try
                {
                    modes.Text = game.Attributes["modes"].Value.ToString().Replace("\\n", "<br/>");
                }
                catch (Exception ex)
                {
                    modes.Text = "";
                }


                /*Description*/
                try
                {
                    description.Text = game.Attributes["description"].Value.ToString();
                }
                catch (Exception ex)
                {
                    description.Text = "";
                }

            }
            else
            {
                /*redirecciona para a pagina default*/
                Response.Redirect("~/Default.aspx");
            }


        }
        protected void Edit_Game(object sender, EventArgs e)
        {
            /*Redirecciona para a pag de edicao*/
            Response.Redirect("Edit.aspx?game=" + name.Text + "");
        }
        protected void Remove_Game(object sender, EventArgs e)
        {
            /*Remover*/

            XmlDocument xdoc = XmlDataSource.GetXmlDocument();

            XmlElement game = xdoc.SelectSingleNode("games/game[@name= '" + name.Text + "' ]") as XmlElement;

            xdoc.DocumentElement.RemoveChild(game);

            XmlDataSource.Save();

            /*Redirecciona para a pag default*/
            Response.Redirect("~/Default.aspx");
        }
    }
}