﻿<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- Display all games in the XML database. -->
  <xsl:param name="sort">genres</xsl:param>
  <xsl:template match="/">
    <rss>
      <channel>
        <xsl:apply-templates select="rss/channel/item">
          <xsl:sort select="*[name()=$sort]" order="ascending"/>
        </xsl:apply-templates>
      </channel>
    </rss>
  </xsl:template>
  <!-- Display a game in the XML database. -->
  <xsl:template match="item">
    <item>
      <xsl:element name="title">
        <xsl:value-of select="title"/>
      </xsl:element>
      <xsl:element name="link">
        <xsl:value-of select="link"/>
      </xsl:element>
      <xsl:element name="description">
        <xsl:value-of select="description"/>
      </xsl:element>
      <xsl:element name="pubDate">
        <xsl:value-of select="pubDate"/>
      </xsl:element>
    </item>
  </xsl:template>
</xsl:stylesheet>