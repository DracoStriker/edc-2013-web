﻿<%@ Page Title="WebGame" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebGame._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2>Get the latest news about video games</h2>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2>Games News</h2>
    <asp:DropDownList ID="dropdownlist" runat="server" />
    <asp:Button ID="Sort" runat="server" OnClick="Sort_Games" Text="Sort" />
    <!--<table>-->
        <asp:Repeater ID="repeater" runat="server">
            <HeaderTemplate>
                <!--<tr>
                    <th>Title</th>
                    <th>Description</th>
                </tr>-->
            </HeaderTemplate>
            <ItemTemplate>
                <!--<tr>
                    <td>--><h3><%#XPath("title") %></h3><!--</td>-->
                <p><b>Publication Date: </b><%#XPath("pubDate") %></p>
                <p><b>Source: </b><a href="<%#XPath("link") %>"><%#XPath("link") %></a></p>
                    <!--<td>--><p><%#XPath("description") %></p><!--</td>-->
                <!--</tr>-->
            </ItemTemplate>
        </asp:Repeater>
    <!--</table>-->
</asp:Content>
