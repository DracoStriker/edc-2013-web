create schema VideoStore;
go

/*
go
select * from VideoStore.Genre;
select * from VideoStore.Idiom;
select * from VideoStore.Storage;
select * from VideoStore.Movie;
select * from VideoStore.Member;
select * from VideoStore.Product;
select * from VideoStore.Purchase;
select * from VideoStore.MovieGenres;
select * from VideoStore.MovieIdioms;
select * from VideoStore.PurchaseProducts;
select * from VideoStore.Cart;
*/

/* Tabela<Enum> Géneros Filme */

create table VideoStore.Genre (
	name varchar(32) primary key
);

insert into VideoStore.Genre values('Adventure');
insert into VideoStore.Genre values('Comedy');
insert into VideoStore.Genre values('Horror');
insert into VideoStore.Genre values('Romance');
insert into VideoStore.Genre values('Action');
insert into VideoStore.Genre values('Drama');
insert into VideoStore.Genre values('Biobraphy');
insert into VideoStore.Genre values('Thriller');
insert into VideoStore.Genre values('Crime');
insert into VideoStore.Genre values('Sport');

/* Tabela<Enum> Linguas */

create table VideoStore.Idiom (
	name varchar(32) primary key
);

insert into VideoStore.Idiom values('Portuguese');
insert into VideoStore.Idiom values('Spanish');
insert into VideoStore.Idiom values('French');
insert into VideoStore.Idiom values('English');
insert into VideoStore.Idiom values('Italian');
/* Tabela<Enum> Armazenamento */

create table VideoStore.Storage (
	name varchar(16) primary key
);

insert into VideoStore.Storage values('DVD');
insert into VideoStore.Storage values('blu-ray');

/* Tabela Filmes */

create table VideoStore.Movie (
	duration time,
	filmMaker varchar(32),
	name varchar(32) not null,
	nDisks int check(nDisks > 0) not null,
	releaseDate date,
	studio varchar(32) not null,
	summary varchar(256),
	primary key (name, studio)
);
	
/* Tabela Utilizador */

create table VideoStore.Member (
	name varchar(32) primary key,
	pass varchar(16) not null,
	email varchar(64) not null,
	age int check(age >= 13) not null
);

/* Tabela Produto */

create table VideoStore.Product (
	storageName varchar(16) references VideoStore.Storage(name) not null,
	movieName varchar(32) not null,
	movieStudio varchar(32) not null,
	stock int check (stock >= 0) not null,
	price int check(price > 0) not null,
	foreign key (movieName, movieStudio) references VideoStore.Movie (name, studio),
	primary key (storageName, movieName, movieStudio)
);


/* Tabela<N-M> Géneros dos Filmes */

create table VideoStore.MovieGenres (
	genrename varchar(32) references VideoStore.Genre(name) not null,
	movieName varchar(32) not null,
	movieStudio varchar(32) not null,
	foreign key (movieName, movieStudio) references VideoStore.Movie(name, studio),
	primary key (genrename, movieName, movieStudio)
);

/* Tabela<N-M> Línguas dos Filmes */

create table VideoStore.MovieIdioms (
	idiomName varchar(32) references VideoStore.Idiom(name) not null,
	movieName varchar(32) not null,
	movieStudio varchar(32) not null,
	foreign key (movieName, movieStudio) references VideoStore.Movie(name, studio),
	primary key (idiomName, movieName, movieStudio)
);

/* Tabela<N-M> Produtos das Compras */

create table VideoStore.PurchaseProducts (
	productStorage varchar(16) not null,
	productMovie varchar(32) not null,
	productStudio varchar(32) not null,
	username varchar(32) references VideoStore.Member(name) not null,
	qty int check(qty > 0) not null,
	foreign key (productStorage, productMovie, productStudio) references VideoStore.Product(storageName, movieName, movieStudio),
	primary key(productStorage, productMovie, productStudio, username)
);

/*Tabela de Admin*/

create table VideoStore.Administrator(
	username varchar(32) references VideoStore.Member(name),
	primary key(username)
);

------Member

insert into VideoStore.Member values ('hugo','aaaa','hugofrade@ua.pt',21);
insert into VideoStore.Member values('simao','bbbb','simao.reis@ua.pt',21);
insert into VideoStore.Member values('admin','cccc','admin@ua.pt',30);

------Administrator
insert into VideoStore.Administrator values('admin');

------Movie
insert into VideoStore.Movie values('2:14:00','Paul Greengrass','Captain Philips',1,cast(getdate() as date),'Columbia Pictures','The true story of Captain Richard Phillips and the 2009 hijacking by Somali pirates of the US-flagged MV Maersk Alabama, the first American cargo ship to be hijacked in two hundred years.');
insert into VideoStore.Movie values('1:47:00','Robert Rodriguez','Machete Kills',1,cast(getdate() as date),'Open Road Films','The U.S. government recruits Machete to battle his way through Mexico in order to take down an arms dealer who looks to launch a weapon into space.');
insert into VideoStore.Movie values('1:58:00','Carlo Carlei','Romeo and Juliet',1,cast(getdate() as date),'Columbia Pictures','Romeo and Juliet secretly wed despite the sworn contempt their families hold for each another. It is not long, however, before a chain of fateful events changes the lives of both families forever.');
insert into VideoStore.Movie values('1:31:00','Alfonso Cuarón','Gravity',1,cast(getdate() as date),'Heyday Films','A medical engineer and an astronaut work together to survive after an accident leaves them adrift in space.');
insert into VideoStore.Movie values('1:35:00','Logan Miller','Sweetwater ',1,cast(getdate() as date),'ProducerA','In the late 1800s, a fanatical religious leader, a renegade Sheriff, and a former prostitute collide in a blood triangle on the rugged plains of the New Mexico Territory.');
insert into VideoStore.Movie values('2:03:00','Ron Howard','Rush',1,cast(getdate() as date),'Cross Creek Pictures','A re-creation of the merciless 1970s rivalry between Formula One rivals James Hunt and Niki Lauda.');

------MovieIdioms
insert into VideoStore.MovieIdioms values('English','Captain Philips','Columbia Pictures');
insert into VideoStore.MovieIdioms values('Italian','Captain Philips','Columbia Pictures');
insert into VideoStore.MovieIdioms values('Spanish','Captain Philips','Columbia Pictures');
insert into VideoStore.MovieIdioms values('English','Machete Kills','Open Road Films');
insert into VideoStore.MovieIdioms values('Italian','Machete Kills','Open Road Films');
insert into VideoStore.MovieIdioms values('French','Machete Kills','Open Road Films');
insert into VideoStore.MovieIdioms values('English','Romeo and Juliet','Columbia Pictures');
insert into VideoStore.MovieIdioms values('Italian','Romeo and Juliet','Columbia Pictures');
insert into VideoStore.MovieIdioms values('Spanish','Romeo and Juliet','Columbia Pictures');
insert into VideoStore.MovieIdioms values('Portuguese','Romeo and Juliet','Columbia Pictures');
insert into VideoStore.MovieIdioms values('English','Gravity','Heyday Films');
insert into VideoStore.MovieIdioms values('Italian','Gravity','Heyday Films');
insert into VideoStore.MovieIdioms values('Spanish','Gravity','Heyday Films');
insert into VideoStore.MovieIdioms values('English','Sweetwater','ProducerA');
insert into VideoStore.MovieIdioms values('Italian','Sweetwater','ProducerA');
insert into VideoStore.MovieIdioms values('Spanish','Sweetwater','ProducerA');
insert into VideoStore.MovieIdioms values('English','Rush','Cross Creek Pictures');
insert into VideoStore.MovieIdioms values('Italian','Rush','Cross Creek Pictures');
insert into VideoStore.MovieIdioms values('Spanish','Rush','Cross Creek Pictures');

------MovieGenres
insert into VideoStore.MovieGenres values('Action','Captain Philips','Columbia Pictures');
insert into VideoStore.MovieGenres values('Biobraphy','Captain Philips','Columbia Pictures');
insert into VideoStore.MovieGenres values('Drama','Captain Philips','Columbia Pictures');
insert into VideoStore.MovieGenres values('Action','Machete Kills','Open Road Films');
insert into VideoStore.MovieGenres values('Crime','Machete Kills','Open Road Films');
insert into VideoStore.MovieGenres values('Thriller','Machete Kills','Open Road Films');
insert into VideoStore.MovieGenres values('Drama','Romeo and Juliet','Columbia Pictures');
insert into VideoStore.MovieGenres values('Romance','Romeo and Juliet','Columbia Pictures');
insert into VideoStore.MovieGenres values('Drama','Gravity','Heyday Films');
insert into VideoStore.MovieGenres values('Thriller','Gravity','Heyday Films');
insert into VideoStore.MovieGenres values('Thriller','Sweetwater','ProducerA');
insert into VideoStore.MovieGenres values('Action','Rush','Cross Creek Pictures');
insert into VideoStore.MovieGenres values('Biobraphy','Rush','Cross Creek Pictures');
insert into VideoStore.MovieGenres values('Drama','Rush','Cross Creek Pictures');
insert into VideoStore.MovieGenres values('Sport','Rush','Cross Creek Pictures');

------Product
insert into VideoStore.Product values ('DVD','Captain Philips','Columbia Pictures',20,10);
insert into VideoStore.Product values ('Blu-Ray','Captain Philips','Columbia Pictures',20,10);
insert into VideoStore.Product values ('DVD','Machete Kills','Open Road Films',20,10);
insert into VideoStore.Product values ('Blu-Ray','Machete Kills','Open Road Films',20,10);
insert into VideoStore.Product values ('DVD','Romeo and Juliet','Columbia Pictures',20,10);
insert into VideoStore.Product values ('Blu-Ray','Romeo and Juliet','Columbia Pictures',20,10);
insert into VideoStore.Product values ('DVD','Gravity','Heyday Films',20,10);
insert into VideoStore.Product values ('Blu-Ray','Gravity','Heyday Films',20,10);
insert into VideoStore.Product values ('DVD','Sweetwater','ProducerA',20,10);
insert into VideoStore.Product values ('Blu-Ray','Sweetwater','ProducerA',20,10);
insert into VideoStore.Product values ('DVD','Rush','Cross Creek Pictures',20,10);
insert into VideoStore.Product values ('Blu-Ray','Rush','Cross Creek Pictures',20,10);

------PurchaseProducts

insert into VideoStore.PurchaseProducts values ('DVD','Captain Philips','Columbia Pictures','hugo',1);
insert into VideoStore.PurchaseProducts values ('Blu-Ray','Machete Kills','Open Road Films','simao',1);
insert into VideoStore.PurchaseProducts values ('Blu-Ray','Romeo and Juliet','Columbia Pictures','hugo',1);
insert into VideoStore.PurchaseProducts values ('Blu-Ray','Rush','Cross Creek Pictures','simao',1);

------------Stored Procedures--------------------

------------User queries------------

go
if object_id('validate_registry') is not null
	drop proc validate_registry

go
create proc validate_registry @username varchar(16), @pwd varchar(16), @mail varchar(64), @age int, @return varchar(5) output as
	select *
	from VideoStore.Member
	where name = @username
	if(@@rowcount = 0)
		begin
			insert into VideoStore.Member (name, pass, email, age) values (@username, @pwd, @mail, @age)
			set @return = 'True'
		end
	else
		set @return = 'False'

go
if object_id('validate_login') is not null
	drop proc validate_login

go
create proc validate_login @username varchar(16), @pwd varchar(16), @return varchar(5) output as
	if(exists(	select *
				from VideoStore.Member
				where name = @username and pass = @pwd))
		set @return = 'True'
	else
		set @return = 'False'

go
if object_id('update_profile') is not null
	drop proc update_profile
go
create proc update_profile @username varchar(16), @email varchar(64), @age int, @return varchar(5) output as
	if(exists(	select *
				from VideoStore.Member
				where name = @username ))
		begin
			update VideoStore.Member set email = @email, age = @age where name = @username
			set @return = 'True'
		end
	else
		set @return = 'False'
go
if object_id('get_profile') is not null
	drop proc get_profile

go
create proc get_profile @username varchar(16) as
	select *
	from VideoStore.Member
	where name = @username;

go
if object_id('validate_pwd_reset') is not null
	drop proc validate_pwd_reset

go
create proc validate_pwd_reset @username varchar(16), @pwd varchar(16), @new_pwd varchar(16), @return varchar(5) output as
	update VideoStore.Member
	set pass = @new_pwd
	where name = @username and pass = @pwd
	if (@@rowcount = 0)
		set @return = 'False'
	else
		set @return = 'True'


------------Admin queries------------
go
if object_id('check_user_is_admin') is not null
	drop proc check_user_is_admin

go
create proc check_user_is_admin @user varchar(16), @return varchar(5) output as
	if(exists(select * from VideoStore.Administrator where username = @user))
		set @return = 'True';
	else
		set @return = 'False'; 


go
if object_id('add_movie') is not null
	drop proc add_movie

go
create proc add_movie @duration time, @filmMaker varchar(32), @name varchar(32), @nDisks int , @releaseDate date, @studio varchar(32), @summary varchar(256), @return varchar(5) output as
	if(exists(	select *
				from VideoStore.Movie
				where name = @name and studio=@studio))
		set @return= 'False'
	else
		begin
		insert into VideoStore.Movie(duration, filmMaker, name, nDisks, releaseDate, studio, summary) values (@duration, @filmMaker, @name, @nDisks, @releaseDate, @studio, @summary)
		set @return= 'True'
		end


go
if object_id('add_product') is not null
	drop proc add_product

go
create proc add_product @storageName varchar(16), @movieName varchar(32), @movieStudio varchar(32), @stock int, @price int, @return varchar(5) output as
	set @return= 'False'
	if(exists(	select *
				from VideoStore.Product
				where storageName = @storageName and movieName = @movieName and movieStudio = @movieStudio ))
		begin
			declare @num int
			select @num = stock from VideoStore.Product where storageName = @storageName and movieName = @movieName and movieStudio = @movieStudio
			set @num =	@num + @stock
			update VideoStore.Product
			set stock = @num, price = @price
			where storageName = @storageName and movieName = @movieName and movieStudio = @movieStudio;
		end
	else
		begin
		insert into VideoStore.Product values (@storageName, @movieName, @movieStudio, @stock,@price);
		end
	set @return= 'True'


------------Movie Queries------------
go
if object_id('get_movie_by_name_studio_storage') is not null
	drop proc get_movie_by_name_studio_storage

go
create proc get_movie_by_name_studio_storage @name varchar(32), @studio varchar(32), @storage varchar(32) as
select distinct duration, filmMaker, name, nDisks, releaseDate, studio, summary, price, stock 
from VideoStore.Movie join VideoStore.Product on (name = movieName and studio=movieStudio)
where name = @name and studio=@studio and storageName=@storage;


go
if object_id('get_movie_by_name') is not null
	drop proc get_movie_by_name

go
create proc get_movie_by_name @name varchar(32), @studio varchar(32) as
	select duration, filmMaker, name, nDisks, releaseDate, studio, summary, price, storageName, stock 
	from VideoStore.Movie join VideoStore.Product on (name = moviename and studio = movieStudio)
	where name = @name and studio = @studio;

go
if object_id('get_movies') is not null
	drop proc get_movies

go
create proc get_movies as
	select distinct duration, filmMaker, name, nDisks, releaseDate, studio, summary
	from VideoStore.Movie

go 
if object_id('get_movies_by_storagename') is not null
	drop proc get_movies_by_storagename

go
create proc get_movies_by_storagename @storagename varchar (32) as
	select storageName, movieName, movieStudio
	from VideoStore.Product join VideoStore.Storage on (VideoStore.Storage.name = VideoStore.Product.storageName)
	where VideoStore.Product.storageName = @storagename;

go
if object_id('get_movies_by_genre') is not null
	drop proc get_movies_by_genre

go
create proc get_movies_by_genre @gen varchar(32) as
	select distinct duration, filmMaker, name, nDisks, releaseDate, studio, summary, price, stock
	from	(select duration, filmMaker, name, nDisks, releaseDate, studio, summary, storageName, price, stock
										from VideoStore.Movie join VideoStore.Product on name = moviename and studio = movieStudio) as temp join VideoStore.MovieGenres on (movieName = name and movieStudio = studio)
	where VideoStore.MovieGenres.genrename = @gen;


go
if object_id('get_movies_by_genre_and_storageName') is not null
	drop proc get_movies_by_genre_and_storageName

go
create proc get_movies_by_genre_and_storageName @gen varchar(32), @storage varchar(32) as
	select duration, filmMaker, name, nDisks, releaseDate, studio, summary, storageName, stock, price
	from	(select duration, filmMaker, name, nDisks, releaseDate, studio, summary, storageName, stock, price
										from VideoStore.Movie join VideoStore.Product on name = moviename and studio = movieStudio) as temp join VideoStore.MovieGenres on (movieName = name and movieStudio = studio)
	where VideoStore.MovieGenres.genrename = @gen and storageName = @storage;


go
if object_id('get_movies_by_releaseDate') is not null
	drop proc get_movies_by_releaseDate

go
create proc get_movies_by_releaseDate as
	select	name, releaseDate, studio
	from	VideoStore.Movie
	order by releaseDate;

go
if object_id('get_purchasedproducts_from_user') is not null
	drop proc get_purchasedproducts_from_user

go
create proc get_purchasedproducts_from_user @userName varchar(32)as
	select distinct productStorage, productMovie, productStudio, price, qty 
	from  VideoStore.PurchaseProducts join VideoStore.Product on (storageName = productStorage and movieName = productMovie and movieStudio = productStudio)
	where userName = @userName;

go
if object_id('add_to_purchasedproducts') is not null
	drop proc add_to_purchasedproducts

go
create proc add_to_purchasedproducts @user varchar(32), @productStorage varchar(32), @productMovie varchar(32), @productStudio varchar(32), @return varchar(5) output as
	
	declare @stock int;
		select @stock = stock from VideoStore.Product where storageName = @productStorage and
		  movieName = @productMovie and movieStudio = @productStudio;
		  if(@stock > 0)
			set @return = 'True';
		  else
			set @return = 'False';
	if(@return = 'True')
	begin
		if(exists(select * from VideoStore.PurchaseProducts 
					where username = @user and productStorage = @productStorage and 
					productMovie = @productMovie and productStudio = @productStudio))
		begin
			update VideoStore.PurchaseProducts
			set qty = qty + 1
			where username = @user and productStorage = @productStorage and 
					productMovie = @productMovie and productStudio = @productStudio;
		end
		else
		begin
			insert into VideoStore.PurchaseProducts values(@productStorage,@productMovie,@productStudio,@user,1);
		end

		update VideoStore.Product
			set stock = stock - 1
			where storageName = @productStorage and 
					movieName = @productMovie and movieStudio = @productStudio;

	end

go 
if object_id('get_movies_by_storagename') is not null
	drop proc get_movies_by_storagename

go
create proc get_movies_by_storagename @storagename varchar (32) as
	select storageName, movieName, movieStudio, price, stock
	from VideoStore.Product join VideoStore.Storage on (VideoStore.Storage.name = VideoStore.Product.storageName)
	where VideoStore.Product.storageName = @storagename