﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoStore
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string usertype = (string)Session["Usertype"] ?? "";
            if (usertype.CompareTo("Member") != 0)
            {
                Response.Redirect("~/MyAccount/MyLogin");
            }
            Dictionary<string, string> profile = GetProfile();
            if (Email.Text.CompareTo("") == 0 && Age.Text.CompareTo("") == 0)
            {
                Email.Text = profile["email"];
                Age.Text = profile["age"];
            }
        }

        private Dictionary<string, string> GetProfile()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("get_profile", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@username", Session["Username"]));
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            Dictionary<string, string> profile = new Dictionary<string, string>();
            profile["email"] = reader["email"].ToString();
            profile["age"] = reader["age"].ToString();
            reader.Close();
            conn.Close();
            return profile;
        }

        protected void Edit_Profile(object sender, EventArgs e)
        {
            try
            {
                int age = Convert.ToInt32(Age.Text);
                if (age < 13)
                {
                    ErrorMessage.Text = "You must be at least 13 years old!";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = "You age is invalid.";
                return;
            }
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            if (Password.Text.CompareTo("") != 0)
            {
                if ((NewPassword.Text.CompareTo(ConfirmNewPassword.Text) == 0) && (NewPassword.Text.CompareTo("") != 0))
                {
                    SqlCommand cmd = new SqlCommand("validate_pwd_reset", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@username", Session["Username"]));
                    cmd.Parameters.Add(new SqlParameter("@pwd", Password.Text));
                    cmd.Parameters.Add(new SqlParameter("@new_pwd", NewPassword.Text));
                    cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
                    {
                        Direction = ParameterDirection.Output,
                        Size = 5
                    });
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    if (cmd.Parameters["@return"].Value.ToString().CompareTo("True") != 0)
                    {
                        ErrorMessage.Text = "Cannot update the password.";
                        return;
                    }
                }
                else
                {
                    ErrorMessage.Text = "Cannot update the password.";
                    return;
                }
             }
             Dictionary<string, string> profile = GetProfile();
             if ((profile["email"].CompareTo(Email.Text) != 0) || (profile["age"].CompareTo(Age.Text) != 0))
             {
                 String email = Email.Text, age = Age.Text;
                 if (Email.Text.CompareTo("") == 0)
                 {
                     email = profile["email"];
                 }
                 else if (Age.Text.CompareTo("") == 0)
                 {
                     age = profile["age"];
                 }
                 SqlCommand cmd = new SqlCommand("update_profile", conn)
                 {
                     CommandType = CommandType.StoredProcedure
                 };
                 cmd.Parameters.Add(new SqlParameter("@username", Session["Username"]));
                 cmd.Parameters.Add(new SqlParameter("@email", email));
                 cmd.Parameters.Add(new SqlParameter("@age", age));
                 cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
                 {
                     Direction = ParameterDirection.Output,
                     Size = 5
                 });
                 conn.Open();
                 cmd.ExecuteNonQuery();
                 conn.Close();
                 if (cmd.Parameters["@return"].Value.ToString() != "True")
                 {
                     ErrorMessage.Text = "Error on profile update.";
                     return;
                 }
             }
             ErrorMessage.Text = "Profile updated with success!";
        }
    }
}