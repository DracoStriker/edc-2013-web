﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoStore.MyAccount
{
    public partial class MyLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string usertype = (string)Session["Usertype"] ?? "";
            if (usertype.CompareTo("") != 0)
            {
                Response.Redirect("~/");
            }
        }

        protected void Check_Login(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("validate_login", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@username", UserName.Text));
            cmd.Parameters.Add(new SqlParameter("@pwd", Password.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                cmd = new SqlCommand("check_user_is_admin", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@user", UserName.Text));
                cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
                {
                    Direction = ParameterDirection.Output,
                    Size = 5
                });
                cmd.ExecuteNonQuery();
                conn.Close();
                Session["Username"] = UserName.Text;
                Session.Timeout = 1;
                if (cmd.Parameters["@return"].Value.ToString() == "True")
                {
                    Session["Usertype"] = "Admin";
                    Response.Redirect("~/Manage.aspx");
                }
                else
                {
                    Session["Usertype"] = "Member";
                    Response.Redirect("~/Profile.aspx");
                }
            }
            else
            {
                ErrorMessage.Text = "Invalid Username or Password.";
                conn.Close();
            }
        }
    }
}