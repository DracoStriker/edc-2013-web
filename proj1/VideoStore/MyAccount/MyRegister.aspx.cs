﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoStore.MyAccount
{
    public partial class MyRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string usertype = (string)Session["Usertype"] ?? "";
            if (usertype.CompareTo("") != 0)
            {
                Response.Redirect("~/");
            }
        }

        protected void Check_Register(object sender, EventArgs e)
        {
            try
            {
                int age = Convert.ToInt32(Age.Text);
                if (age < 13)
                {
                    ErrorMessage.Text = "You must be at least 13 years old!";
                    return;
                }
            }
            catch(Exception ex)
            {
                ErrorMessage.Text = "You age is invalid.";
                return;
            }
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("validate_registry", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@username", UserName.Text));
            cmd.Parameters.Add(new SqlParameter("@pwd", Password.Text));
            cmd.Parameters.Add(new SqlParameter("@mail", Email.Text));
            cmd.Parameters.Add(new SqlParameter("@age", Age.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                Response.Redirect("~/MyAccount/MyLogin.aspx");
            }
            else
            {
                ErrorMessage.Text = "Username already exists.";
            }
        }
    }
}