﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace VideoStore
{
    public partial class Manage2 : System.Web.UI.Page
    {
        private static readonly Regex date = new Regex(@"\d{1,2}[-/]\d{1,2}[-/]\d{4}");
        private static readonly Regex time = new Regex(@"\d{2}:\d{2}:\d{2}");
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string usertype = (string)Session["Usertype"] ?? "";
            if (usertype.CompareTo("Admin") != 0)
            {
                Response.Redirect("~/MyAccount/MyLogin");
            }
        }

        protected void Add_Movie(object sender, EventArgs e)
        {
            try
            {
                int nDisks = Convert.ToInt32(NDisks.Text);
                if (nDisks < 1)
                {
                    ErrorMessage.Text = "Your product must have at least 1 disk!";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = "Your number of disks is invalid.";
                return;
            }
            if (!date.IsMatch(ReleaseDate.Text))
            {
                ErrorMessage.Text = "Invalid date! Insert dd-mm-yyyy or dd/mm/yyyy.";
                return;
            }
            if (!time.IsMatch(Duration.Text))
            {
                ErrorMessage.Text = "Invalid time! Insert hh:mm:ss.";
                return;
            }
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("add_movie", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@duration", Duration.Text));
            cmd.Parameters.Add(new SqlParameter("@filmMaker", FilmMaker.Text));
            cmd.Parameters.Add(new SqlParameter("@name", MovieName.Text));
            cmd.Parameters.Add(new SqlParameter("@nDisks", NDisks.Text));
            cmd.Parameters.Add(new SqlParameter("@releaseDate", ReleaseDate.Text));
            cmd.Parameters.Add(new SqlParameter("@studio", StudioName.Text));
            cmd.Parameters.Add(new SqlParameter("@summary", Summary.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                ErrorMessage.Text = "Movie added with success!";
            }
            else
            {
                ErrorMessage.Text = "Failed to add movie.";
            }
        }
    }
}