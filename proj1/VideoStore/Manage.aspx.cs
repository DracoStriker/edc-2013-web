﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;

namespace VideoStore
{
    public partial class Manage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string usertype = (string)Session["Usertype"] ?? "";
            if (usertype.CompareTo("Admin") != 0)
            {
                Response.Redirect("~/MyAccount/MyLogin");
            }
            
            if (MovieList.Items.Capacity == 0)
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
                SqlCommand cmd = new SqlCommand("get_movies", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    MovieList.Items.Add(reader["name"].ToString()+", "+reader["studio"].ToString());
                }
                conn.Close();
            }
        }

        protected void Add_Product(object sender, EventArgs e)
        {
            try
            {
                int stock = Convert.ToInt32(Stock.Text);
                if (stock < 1)
                {
                    ErrorMessage.Text = "You must insert at least 1 unit from your product!";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = "Your stock is invalid.";
                return;
            }
            try
            {
                int price = Convert.ToInt32(Price.Text);
                if (price < 1)
                {
                    ErrorMessage.Text = "The product must cost 1€ or more!";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = "Your price is invalid.";
                return;
            }
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("add_product", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            string[] movie = Regex.Split(MovieList.SelectedItem.Value, ", ");
            cmd.Parameters.Add(new SqlParameter("@storagename", StockTypeList.SelectedItem.Value));
            cmd.Parameters.Add(new SqlParameter("@movieName", movie[0]));
            cmd.Parameters.Add(new SqlParameter("@movieStudio", movie[movie.Length-1]));
            cmd.Parameters.Add(new SqlParameter("@stock", Stock.Text));
            cmd.Parameters.Add(new SqlParameter("@price", Price.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                ErrorMessage.Text = "Product added with success!";
            }
            else
            {
                ErrorMessage.Text = "Failed to add product.";
            }
        }
    }
}