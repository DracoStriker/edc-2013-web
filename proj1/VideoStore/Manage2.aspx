﻿<%@ Page Title="Manage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage2.aspx.cs" Inherits="VideoStore.Manage2" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Add new movies to your website.</h2>
    </hgroup>
    <h2>Add a Movie</h2>
    <section id="addmovieForm">
        <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
        <fieldset>
            <legend>Add Movie Form</legend>
            <ol>
                <li>
                    <asp:Label runat="server" AssociatedControlID="MovieName">Movie Name</asp:Label>
                    <asp:TextBox runat="server" ID="MovieName" />
                    <asp:RequiredFieldValidator ID="MovieNameValidator" runat="server" ControlToValidate="MovieName" CssClass="field-validation-error" ErrorMessage="The movie name field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="StudioName">Studio Name</asp:Label>
                    <asp:TextBox runat="server" ID="StudioName" />
                    <asp:RequiredFieldValidator ID="StudioNameValidator" runat="server" ControlToValidate="StudioName" CssClass="field-validation-error" ErrorMessage="The studio name field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Duration">Duration</asp:Label>
                    <asp:TextBox runat="server" ID="Duration" />
                    <asp:RequiredFieldValidator ID="DurationValidator" runat="server" ControlToValidate="Duration" CssClass="field-validation-error" ErrorMessage="The duration field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="FilmMaker">Film Maker</asp:Label>
                    <asp:TextBox runat="server" ID="FilmMaker" />
                    <asp:RequiredFieldValidator ID="FilmMakerValidator" runat="server" ControlToValidate="FilmMaker" CssClass="field-validation-error" ErrorMessage="The film maker field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="NDisks">Number of Disks</asp:Label>
                    <asp:TextBox runat="server" ID="NDisks" />
                    <asp:RequiredFieldValidator ID="NDisksValidator" runat="server" ControlToValidate="NDisks" CssClass="field-validation-error" ErrorMessage="The number of disks field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="ReleaseDate">Release Date</asp:Label>
                    <asp:TextBox runat="server" ID="ReleaseDate" />
                    <asp:RequiredFieldValidator ID="ReleaseDateValidator" runat="server" ControlToValidate="ReleaseDate" CssClass="field-validation-error" ErrorMessage="The release date field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Summary">Summary</asp:Label>
                    <asp:TextBox runat="server" ID="Summary" TextMode="multiline"/>
                    <asp:RequiredFieldValidator ID="SummaryValidator" runat="server" ControlToValidate="Summary" CssClass="field-validation-error" ErrorMessage="The sumamry field is required." />
                </li>
            </ol>
            <asp:Button ID="AddmovieButton" runat="server" Text="Add movie" OnClick="Add_Movie"/>
        </fieldset>
    </section>
</asp:Content>
