﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="VideoStore.Contact" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Our contact page.</h2>
    </hgroup>

    <section class="contact">
        <header>
            <h3>Simão Reis:</h3>
        </header>
        <p>
            <span class="label">NMec:</span>
            <span>59575</span>
        </p>
        <p>
            <span class="label">Mail:</span>
            <span><a href="mailto:simao.paulo@ua.pt">simao.paulo@ua.pt</a></span>
        </p>
        <header>
            <h3>Hugo Frade:</h3>
        </header>
        <p>
            <span class="label">NMec:</span>
            <span>59399</span>
        </p>
        <p>
            <span class="label">Mail:</span>
            <span><a href="mailto:hugofrade@ua.pt">hugofrade@ua.pt</a></span>
        </p>
    </section>
</asp:Content>