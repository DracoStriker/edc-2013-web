﻿<%@ Page Title="Manage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="VideoStore.Manage" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Add new products to your website.</h2>
    </hgroup>
    <h2>Add a Product</h2>
    <section id="addproductForm">
        <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
        <fieldset>
            <legend>Add Product Form</legend>
            <ol>
                <li>
                    <asp:Label runat="server">Movie and Studio name</asp:Label><br />
                    <asp:DropDownList ID="MovieList" runat="server">
                    </asp:DropDownList>
                </li>
                <li>
                    <asp:Label runat="server">Content type</asp:Label><br />
                    <asp:DropDownList ID="StockTypeList" runat="server">
                        <asp:ListItem Text="DVD" Value="DVD"/>
                        <asp:ListItem Text="Blu-ray" Value="blu-ray"/>
                    </asp:DropDownList>
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Stock">Stock Quantity</asp:Label>
                    <asp:TextBox runat="server" ID="Stock" />
                    <asp:RequiredFieldValidator ID="StockValidator" runat="server" ControlToValidate="Stock" CssClass="field-validation-error" ErrorMessage="The stock name field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Price">Price</asp:Label>
                    <asp:TextBox runat="server" ID="Price" />
                    <asp:RequiredFieldValidator ID="PriceValidator" runat="server" ControlToValidate="Price" CssClass="field-validation-error" ErrorMessage="The price field is required." />
                </li>
            </ol>
            <asp:Button ID="AddproductButton" runat="server" Text="Add product" OnClick="Add_Product"/>
        </fieldset>
    </section>
</asp:Content>
