﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="VideoStore.Product" %>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ListView ID ="movie" runat="server">
        <ItemTemplate>
            <table>
                <fieldset>
                    <tr>
                        <td>
                            Movie Name: <%#Eval("name") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Studio name: <%#Eval("studio") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Film Maker: <%#Eval("filmmaker") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Duration: <%#Eval("duration") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Release Date: <%#Eval("releaseDate") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Number of Disks: <%#Eval("nDisks") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Brief Description: <%#Eval("summary") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Available Copies: <%#Eval("stock") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Format: <%#Eval("storage") %>;
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Price: <%#Eval("price") %> €;
                        </td>
                    </tr>
                </fieldset>
            </table>
        </ItemTemplate>
    </asp:ListView>
    <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <asp:Button ID="Purchase_Button" Visible="false" runat="server" OnClick="Purchase_Product" Text="Buy">
    </asp:Button>
</asp:Content>
