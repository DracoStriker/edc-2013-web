﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="VideoStore.About" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Our app description page.</h2>
    </hgroup>

    <article>
        <p>        
            This app was developed for the course of Engenharia de Dados e do Conhecimento.
        </p>
    </article>
</asp:Content>