﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="VideoStore.Orders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <section class="featured">
        <div class="content-wrapper">
            <ul>
                <asp:ListView ID="ProductList" runat="server" ItemType="VideoStore.MyModels.PurchasedProduct" itemPlaceholder="">
                    <EmptyDataTemplate>      
                            <table id="Table1" runat="server">        
                                <tr><td>You didn't bought anything yet.</td></tr>
                            </table>  
                        </EmptyDataTemplate>  
                        <EmptyItemTemplate>     
                            <td id="Td1" runat="server" />  
                        </EmptyItemTemplate>  
                        <GroupTemplate>    
                            <tr ID="itemPlaceholderContainer" runat="server">      
                                <td ID="itemPlaceholder" runat="server"></td>    
                            </tr>  
                        </GroupTemplate>  
                    <ItemTemplate>
                        <td id="Td2" runat="server">      
                            <table>        
                                <tr>          
                                    <td>&nbsp;</td>          
                                    <td>          
                                        <br />
                                        <span>           
                                            <b>Studio: </b><%# Eval("Name")%>
                                        </span>
                                        <span>           
                                            <b>Studio: </b><%# Eval("Studio")%>
                                        </span>
                                        <span>           
                                            <b>Format: </b><%# Eval("Storage")%>
                                        </span>
                                        <span>           
                                            <b>Price: </b><%# Eval("Price")%>
                                        </span>
                                        <span>           
                                            <b>Quantity: </b><%# Eval("Qty")%>
                                        </span>
                                        <br />            
                                    </td>        
                                </tr>      
                            </table>    
                        </td>  
                    </ItemTemplate>
                    <LayoutTemplate>    
                            <table id="Table2" runat="server">      
                                <tr id="Tr1" runat="server">        
                                    <td id="Td3" runat="server">          
                                        <table ID="groupPlaceholderContainer" runat="server">            
                                            <tr ID="groupPlaceholder" runat="server"></tr>          
                                        </table>        
                                    </td>      
                                </tr>      
                                <tr id="Tr2" runat="server"><td id="Td4" runat="server"></td></tr>    
                            </table>  
                        </LayoutTemplate>
                </asp:ListView>
            </ul>
        </div>
    </section>
</asp:Content>
