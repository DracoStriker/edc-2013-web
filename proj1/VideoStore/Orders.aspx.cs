﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VideoStore.MyModels;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoStore
{
    public partial class Orders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string usertype = (string)Session["Usertype"] ?? "";
            if (usertype.CompareTo("Member") != 0)
            {
                Response.Redirect("~/MyAccount/MyLogin");
            }
            List<PurchasedProduct> products = new List<PurchasedProduct>();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("get_purchasedproducts_from_user", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@userName", Session["username"]));
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                products.Add(new PurchasedProduct(reader["productMovie"].ToString(), reader["productStudio"].ToString(), reader["productStorage"].ToString(), reader["price"].ToString(), reader["qty"].ToString()));
            }
            reader.Close();
            conn.Close();
            ProductList.DataSource = products;
            ProductList.DataBind();
        }
    }
}