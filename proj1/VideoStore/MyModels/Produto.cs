﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoStore.MyModels
{
    public class Produto
    {
        public string name { get; set; }
        public string studio { get; set; }
        public string filmmaker { get; set; }
        public string duration { get; set; }
        public string releaseDate { get; set; }
        public string nDisks { get; set; }
        public string summary { get; set; }
        public string storage { get; set; }
        public int stock { get; set; }
        public int price { get; set; }
        public Produto(string n, string s, string f, string d, string r, string ndi, string sumary, string st, int sto, int pr)
        {
            name = n;
            studio = s;
            filmmaker = f;
            duration = d;
            releaseDate = r;
            nDisks = ndi;
            summary = sumary;
            storage = st;
            stock = sto;
            price = pr;
        }
    }
}