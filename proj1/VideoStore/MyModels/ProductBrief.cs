﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoStore.MyModels
{
    public class ProductBrief
    {
        public string Name { get; set; }
        public string Studio { get; set; }
        public string Stock { get; set; }
        public string Price { get; set; }
        
        public ProductBrief(string name, string studio, string stock, string price)
        {
            this.Name = name;
            this.Studio = studio;
            this.Stock = stock;
            this.Price = price;
        }
    }
}