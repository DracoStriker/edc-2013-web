﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoStore.MyModels
{
    public class PurchasedProduct
    {
        public string Name { get; set; }
        public string Studio { get; set; }
        public string Storage { get; set; }
        public string Price { get; set; }
        public string Qty { get; set; }

        public PurchasedProduct(string name, string studio, string storage, string price, string qty)
        {
            this.Name = name;
            this.Studio = studio;
            this.Storage = storage;
            this.Price = price;
            this.Qty = qty;
        }
    }
}