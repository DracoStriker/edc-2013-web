﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VideoStore.MyModels;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoStore
{
    public partial class Dvd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<ProductBrief> products = new List<ProductBrief>();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            if ((Request.QueryString["Genre"] ?? "").CompareTo("") != 0)
            {
                SqlCommand cmd = new SqlCommand("get_movies_by_genre_and_storageName", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@gen", Request.QueryString["Genre"]));
                cmd.Parameters.Add(new SqlParameter("@storage", "DVD"));
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    products.Add(new ProductBrief(reader["name"].ToString(), reader["studio"].ToString(), reader["stock"].ToString(), reader["price"].ToString()));
                }
                reader.Close();
                conn.Close();
            }
            else
            {
                SqlCommand cmd = new SqlCommand("get_movies_by_storagename", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@storagename", "DVD"));
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    products.Add(new ProductBrief(reader["movieName"].ToString(), reader["movieStudio"].ToString(), reader["stock"].ToString(), reader["price"].ToString()));
                }
                reader.Close();
                conn.Close();
            }
            DVDList.DataSource = products;
            DVDList.DataBind();
        }
    }
}