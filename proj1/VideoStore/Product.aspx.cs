﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VideoStore.MyModels;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace VideoStore
{
    public partial class Product : System.Web.UI.Page
    {
        List<Produto> p;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.QueryString["movieName"] ?? "").CompareTo("")==0
                || (Request.QueryString["studio"] ?? "").CompareTo("") == 0
                || (Request.QueryString["storage"] ?? "").CompareTo("") == 0)
            {
                Response.Redirect("~/");
            }
            String name = Request.QueryString["movieName"];
            String studio = Request.QueryString["studio"];
            String storage = Request.QueryString["storage"];
            p = new List<Produto>();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("get_movie_by_name_studio_storage", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@name", name);
            cmd.Parameters.Add("@studio", studio);
            cmd.Parameters.Add("@storage", storage);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            p.Add(new Produto(name, studio, reader["filmMaker"].ToString(), reader["duration"].ToString(), reader["releaseDate"].ToString(),
                reader["nDisks"].ToString(), reader["summary"].ToString(), storage, Convert.ToInt16(reader["stock"].ToString()),
                Convert.ToInt16(reader["price"].ToString())));

            conn.Close();
            movie.DataSource = p;
            movie.DataBind();
            if (((string)Session["usertype"] ?? "").CompareTo("Member") == 0)
            {
                Purchase_Button.Visible = true;
            }
        }

        protected void Purchase_Product(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("add_to_purchasedproducts", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add("@user", Session["username"]);
            cmd.Parameters.Add("@productStorage", p[0].storage);
            cmd.Parameters.Add("@productMovie", p[0].name);
            cmd.Parameters.Add("@productStudio", p[0].studio);
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                ErrorMessage.Text = "Item purchased with success!";
            }
            else
            {
                ErrorMessage.Text = "There is no more stock. Try another time.";
            }
        }
    }
}