﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Bluray.aspx.cs" Inherits="VideoStore.Bluray" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <section class="featured">
        <div class="content-wrapper">
            <ul>
                <asp:ListView ID="DVDList" runat="server" ItemType="VideoStore.MyModels.ProductBrief" itemPlaceholder="">
                    <EmptyDataTemplate>      
                            <table id="Table1" runat="server">        
                                <tr><td>No movies of the selected genre exists.</td></tr>
                            </table>  
                        </EmptyDataTemplate>  
                        <EmptyItemTemplate>     
                            <td id="Td1" runat="server" />  
                        </EmptyItemTemplate>  
                        <GroupTemplate>    
                            <tr ID="itemPlaceholderContainer" runat="server">      
                                <td ID="itemPlaceholder" runat="server"></td>    
                            </tr>  
                        </GroupTemplate>  
                    <ItemTemplate>
                        <td id="Td2" runat="server">      
                            <table>        
                                <tr>          
                                    <td>&nbsp;</td>          
                                    <td>
                                        <a href="Product.aspx?movieName=<%#:Eval("name")%>&studio=<%#Eval("studio") %>&storage=blu-ray">
                                            <span class="ProductName"><%# Eval("name")%></span>
                                        </a>            
                                        <br />
                                        <span>           
                                            <b>Studio: </b><%# Eval("studio")%>
                                        </span>
                                        <span>           
                                            <b>Stock: </b><%# Eval("stock")%>
                                        </span>
                                        <span>           
                                            <b>Price: </b><%# Eval("price")%>
                                        </span>
                                        <br />            
                                    </td>        
                                </tr>      
                            </table>    
                        </td>  
                    </ItemTemplate>
                    <LayoutTemplate>    
                            <table id="Table2" runat="server">      
                                <tr id="Tr1" runat="server">        
                                    <td id="Td3" runat="server">          
                                        <table ID="groupPlaceholderContainer" runat="server">            
                                            <tr ID="groupPlaceholder" runat="server"></tr>          
                                        </table>        
                                    </td>      
                                </tr>      
                                <tr id="Tr2" runat="server"><td id="Td4" runat="server"></td></tr>    
                            </table>  
                        </LayoutTemplate>
                </asp:ListView>
            </ul>
        </div>
    </section>
    <aside>
        <h3>Search available Blu-ray's by genre.</h3>
        <ul>
            <li><a id="A1" runat="server" href="~/Bluray">All</a></li>
            <li><a id="A2" runat="server" href="~/Bluray.aspx?Genre=Action">Action</a></li>
            <li><a id="A3" runat="server" href="~/Bluray.aspx?Genre=Adventure">Adventure</a></li>
            <li><a id="A4" runat="server" href="~/Bluray.aspx?Genre=Biography">Biography</a></li>
            <li><a id="A5" runat="server" href="~/Bluray.aspx?Genre=Comedy">Comedy</a></li>
            <li><a id="A6" runat="server" href="~/Bluray.aspx?Genre=Crime">Crime</a></li>
            <li><a id="A7" runat="server" href="~/Bluray.aspx?Genre=Drama">Drama</a></li>
            <li><a id="A8" runat="server" href="~/Bluray.aspx?Genre=Horror">Horror</a></li>
            <li><a id="A9" runat="server" href="~/Bluray.aspx?Genre=Romance">Romance</a></li>
            <li><a id="A10" runat="server" href="~/Bluray.aspx?Genre=Sport">Romance</a></li>
            <li><a id="A11" runat="server" href="~/Bluray.aspx?Genre=Thriller">Thriller</a></li>
        </ul>
    </aside>
</asp:Content>
