﻿<%@ Page Title="My Profile" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="VideoStore.Profile" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h2>Use the form below to change your profile information.</h2>
        <br/>
        <asp:Label id="YourName" runat="server" Font-Size="Large"></asp:Label>
    </hgroup>
    <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <fieldset>
        <legend>Edit Profile Form</legend>
        <ol>
            <li>
                <asp:Label ID="Label2" runat="server" AssociatedControlID="Email">Email address</asp:Label>
                <asp:TextBox runat="server" ID="Email" TextMode="Email" />
                <asp:RequiredFieldValidator ID="EmailValidator" runat="server" ControlToValidate="Email"
                    CssClass="field-validation-error" ErrorMessage="The email address field is required." />
            </li>
            <li>
                <asp:Label ID="Label3" runat="server" AssociatedControlID="Age">Age</asp:Label>
                <asp:TextBox runat="server" ID="Age" />
                <asp:RequiredFieldValidator ID="AgeValidator" runat="server" ControlToValidate="Age"
                    CssClass="field-validation-error" ErrorMessage="The age field is required." />

            </li>
            <li>
                <asp:Label ID="Label1" runat="server" AssociatedControlID="Password">Old Password</asp:Label>
                <asp:TextBox runat="server" ID="Password" TextMode="Password" />
           </li>
           <li>
                <asp:Label ID="Label4" runat="server" AssociatedControlID="NewPassword">New password</asp:Label>
                <asp:TextBox runat="server" ID="NewPassword" TextMode="Password" /> 
           </li>
           <li>
                <asp:Label ID="Label5" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm new password</asp:Label>
                <asp:TextBox runat="server" ID="ConfirmNewPassword" TextMode="Password" /> 
           </li>
        </ol>
        <asp:Button ID="EditProfile" runat="server" OnClick="Edit_Profile" Text="Edit Profile" />
    </fieldset>



</asp:Content>