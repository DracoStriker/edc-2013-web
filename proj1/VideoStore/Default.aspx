﻿<%@ Page Title="Welcome" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="VideoStore._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2>Start searching for your favorite movies.</h2>
            </hgroup>
            <p>
                Here you can find the most recents movies on the market. <br />
                Register now so you can start ordering now!
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <!--<h3>Highlights of the week:</h3>
    <ol class="round">
        <li class="one">
            <h5>Most recent products</h5>
            
        </li>
        <li class="two">
            <h5>Top DVDs of the week</h5>
            
        </li>
        <li class="three">
            <h5>Top blu-rays of the week</h5>
            
        </li>
    </ol>-->
</asp:Content>
