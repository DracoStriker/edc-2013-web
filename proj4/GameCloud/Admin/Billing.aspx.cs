﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace GameCloud.Admin
{
    public partial class Billing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string user_type = (string)Session["user_type"] ?? "";
            if (user_type.CompareTo("Admin") != 0)
            {
                Response.Redirect("~/");
            }
        }

        protected void Check_Billing(object sender, EventArgs e)
        {
            try
            {
                int price = Convert.ToInt32(Cash.Text);
                if (price < 1)
                {
                    ErrorMessage.Text = "The charge must be of 1€ or more!";
                    return;
                }
            }
            catch (Exception)
            {
                ErrorMessage.Text = "Your price is invalid.";
                return;
            }
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("charge_money", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@user", Username.Text));
            cmd.Parameters.Add(new SqlParameter("@money", Cash.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                ErrorMessage.Text = "Billing with success!";
            }
            else
            {
                ErrorMessage.Text = "User does not exist!";
            }
        }
    }
}