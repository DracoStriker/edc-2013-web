﻿<%@ Page Title="New Game" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Stock.aspx.cs" Inherits="GameCloud.Admin.Stock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Add new games.</h2>
    </hgroup>
    <h2>Add a game.</h2>
    <section id="addproductForm">
        <p class="validation-summary-errors">
            <asp:Literal runat="server" ID="ErrorMessage" />
        </p>
        <fieldset>
            <legend>Add Product Form</legend>
            <ol>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Name">Name</asp:Label>
                    <asp:TextBox runat="server" ID="Name" />
                    <asp:RequiredFieldValidator ID="NameValidator" runat="server" ControlToValidate="Name" CssClass="field-validation-error" ErrorMessage="The Name field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Developer">Developer</asp:Label>
                    <asp:TextBox runat="server" ID="Developer" />
                    <asp:RequiredFieldValidator ID="DeveloperValidator" runat="server" ControlToValidate="Developer" CssClass="field-validation-error" ErrorMessage="The Developer field is required." />
                </li>
                <li>
                    <asp:Label runat="server">Genre</asp:Label><br />
                    <asp:DropDownList ID="GenreList" runat="server">
                        <asp:ListItem Text="Online Battle Arena" Value="Online Battle Arena"/>
                        <asp:ListItem Text="RPG" Value="RPG"/>
                        <asp:ListItem Text="Platform" Value="Platform"/>
                        <asp:ListItem Text="Adventure" Value="Adventure"/>
                        <asp:ListItem Text="Action" Value="Action"/>
                    </asp:DropDownList>
                </li>
                <li>
                    <asp:Label runat="server">Release Date</asp:Label><br />
                    <asp:TextBox ID="releaseDate" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regexpDate" runat="server" ControlToValidate="releaseDate" ErrorMessage="Input valid date." ValidationExpression="^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((1[6-9]|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((1[6-9]|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$"></asp:RegularExpressionValidator>
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Price">Price</asp:Label>
                    <asp:TextBox runat="server" ID="Price" />
                    <asp:RequiredFieldValidator ID="PriceValidator" runat="server" ControlToValidate="Price" CssClass="field-validation-error" ErrorMessage="The price field is required." />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Summary">Summary</asp:Label>
                    <asp:TextBox runat="server" ID="Summary" TextMode="multiline"/>
                    <asp:RequiredFieldValidator ID="SummaryValidator" runat="server" ControlToValidate="Summary" CssClass="field-validation-error" ErrorMessage="The summary field is required." />
                </li>
            </ol>
            <asp:Button ID="AddGameButton" runat="server" Text="Add Game" OnClick="Add_Game" />
        </fieldset>
    </section>
</asp:Content>
