﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace GameCloud.Admin
{
    public partial class Stock : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string user_type = (string)Session["user_type"] ?? "";
            if (user_type.CompareTo("Admin") != 0)
            {
                Response.Redirect("~/");
            }
        }

        protected void Add_Game(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            /* obter ids */
            SqlCommand cmd = new SqlCommand("get_existing_games", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<string> listGames = new List<string>();
            while (reader.Read())
            {
                listGames.Add(reader["game"].ToString());
            }
            reader.Close();
            conn.Close();
            foreach (string game in listGames)
            {
                if (game.CompareTo(Name.Text) == 0)
                {
                    ErrorMessage.Text = "Game already exist!";
                    return;
                }
            }
            /* adicionar jogo */
            try
            {
                int price = Convert.ToInt32(Price.Text);
                if (price < 1)
                {
                    ErrorMessage.Text = "The product must cost 1€ or more!";
                    return;
                }
            }
            catch (Exception)
            {
                ErrorMessage.Text = "Your price is invalid.";
                return;
            }
            cmd = new SqlCommand("add_game", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@name", Name.Text));
            cmd.Parameters.Add(new SqlParameter("@developer", Developer.Text));
            cmd.Parameters.Add(new SqlParameter("@genre", GenreList.SelectedItem.Value));
            cmd.Parameters.Add(new SqlParameter("@releaseDate", releaseDate.Text));
            cmd.Parameters.Add(new SqlParameter("@price", Price.Text));
            cmd.Parameters.Add(new SqlParameter("@description", Summary.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                ErrorMessage.Text = "Game added with success!";
            }
            else
            {
                ErrorMessage.Text = "Failed to add game.";
            }
        }
    }
}