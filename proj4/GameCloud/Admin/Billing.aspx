﻿<%@ Page Title="Billing" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Billing.aspx.cs" Inherits="GameCloud.Admin.Billing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2>Add cash to user.</h2>
    </hgroup>
    <section id="BillingForm">
        <fieldset>
            <legend>Billing Form</legend>
            <ol>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Username">Username</asp:Label>
                    <asp:TextBox runat="server" ID="Username" />
                    <asp:RequiredFieldValidator ID="UsernameValidator" runat="server" ControlToValidate="Username" CssClass="field-validation-error" ErrorMessage="The Username field is required." />
                </li>
            </ol>
            <ol>
                <li>
                    <asp:Label runat="server" AssociatedControlID="Cash">Cash</asp:Label>
                    <asp:TextBox runat="server" ID="Cash" />
                    <asp:RequiredFieldValidator ID="CashValidator" runat="server" ControlToValidate="Cash" CssClass="field-validation-error" ErrorMessage="The Cash field is required." />
                </li>
            </ol>
            <asp:Button ID="LogInButton" runat="server" Text="Cash" OnClick="Check_Billing" />
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
        </fieldset>
    </section>
</asp:Content>
