﻿<%@ Page Title="Game Cloud" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GameCloud._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2>Buy games and receive them at your favorite gaming platforms.</h2>
            </hgroup>
            <p>
                We suport all your favorite gaming networks. Start with free 10€ billing.
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3>We suggest the following:</h3>
    <ol class="round">
        <li class="one">
            <h5>Register</h5>
            <a href="MyAccount/Register">Register</a> at our system to be able to login.
        </li>
        <li class="two">
            <h5>Login</h5>
            <a href="MyAccount/Login">Login</a> at our system to check your games.
        </li>
        <li class="three">
            <h5>Check our Games</h5>
            Check our latest games at our complete <a href="Games">game list</a>.
        </li>
    </ol>
</asp:Content>
