﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Games.aspx.cs" Inherits="GameCloud.Games" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <section class="featured">
        <div class="content-wrapper">
            <ul>
                <asp:ListView ID="GameList" runat="server" ItemType="GameCloud.MyModels.Game" itemPlaceholder="">
                    <EmptyDataTemplate>
                        <table id="Table1" runat="server">
                            <tr>
                                <td>No games of the selected genre exists.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <EmptyItemTemplate>
                        <td id="Td1" runat="server" />
                    </EmptyItemTemplate>
                    <GroupTemplate>
                        <tr id="itemPlaceholderContainer" runat="server">
                            <td id="itemPlaceholder" runat="server"></td>
                        </tr>
                    </GroupTemplate>
                    <ItemTemplate>
                        <td id="Td2" runat="server">
                            <table>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <a href="Game.aspx?game=<%#:Eval("id")%>">
                                            <span class="ProductName"><%# Eval("name")%></span>
                                        </a>
                                        <br />
                                        <span>
                                            <b>Price: </b><%# Eval("price")%>
                                        </span>
                                        <span>
                                            <b>Description: </b><%# Eval("description")%>
                                        </span>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </ItemTemplate>
                    <LayoutTemplate>
                        <table id="Table2" runat="server">
                            <tr id="Tr1" runat="server">
                                <td id="Td3" runat="server">
                                    <table id="groupPlaceholderContainer" runat="server">
                                        <tr id="groupPlaceholder" runat="server"></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="Tr2" runat="server">
                                <td id="Td4" runat="server"></td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                </asp:ListView>
            </ul>
        </div>
    </section>
    <aside>
        <h3>Search available Game's by genre.</h3>
        <ul>
            <li><a id="A1" runat="server" href="~/Games">All</a></li>
            <li><a id="A2" runat="server" href="~/Games.aspx?Genre=Online Battle Arena">Online Battle Arena</a></li>
            <li><a id="A3" runat="server" href="~/Games.aspx?Genre=RPG">RPG</a></li>
            <li><a id="A4" runat="server" href="~/Games.aspx?Genre=Platform">Platform</a></li>
            <li><a id="A5" runat="server" href="~/Games.aspx?Genre=Adventure">Adventure</a></li>
            <li><a id="A6" runat="server" href="~/Games.aspx?Genre=Action">Action</a></li>
        </ul>
    </aside>
</asp:Content>
