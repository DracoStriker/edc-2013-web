﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="GameCloud.User.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
        <h2>Use the form below to change your profile information.</h2>
        <asp:Label ID="YourName" runat="server" Font-Size="Large"></asp:Label>
    </hgroup>
    <p class="validation-summary-errors">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <p><asp:Label runat="server">Cash:</asp:Label><br />
        <asp:Label ID="Cash" runat="server" />
    </p>
    <fieldset>
        <legend>Edit Profile</legend>
        <ol>
            <li>
                <asp:Label runat="server" AssociatedControlID="Email">Email Address</asp:Label>
                <asp:TextBox runat="server" ID="Email" TextMode="Email" />
                <asp:RequiredFieldValidator ID="EmailValidator" runat="server" ControlToValidate="Email"
                    CssClass="field-validation-error" ErrorMessage="The email address field is required." />
            </li>
            <li>
                <asp:Label runat="server">Currency</asp:Label><br />
                <asp:DropDownList ID="CurrencyList" runat="server">
                    <asp:ListItem Text="EUR" Value="EUR"/>
                    <asp:ListItem Text="USD" Value="USD"/>
                    <asp:ListItem Text="GBP" Value="GBP"/>
                    <asp:ListItem Text="JPY" Value="JPY"/>
                    <asp:ListItem Text="CAD" Value="CAD"/>
                    <asp:ListItem Text="AUD" Value="AUD"/>
                    <asp:ListItem Text="CNY" Value="CNY"/>
                </asp:DropDownList>
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="Age">Age</asp:Label>
                <asp:TextBox runat="server" ID="Age" />
                <asp:RequiredFieldValidator ID="AgeValidator" runat="server" ControlToValidate="Age"
                    CssClass="field-validation-error" ErrorMessage="The age field is required." />
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="Password">Old Password</asp:Label>
                <asp:TextBox runat="server" ID="Password" TextMode="Password" />
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="NewPassword">New password</asp:Label>
                <asp:TextBox runat="server" ID="NewPassword" TextMode="Password" />
            </li>
            <li>
                <asp:Label runat="server" AssociatedControlID="ConfirmNewPassword">Confirm new password</asp:Label>
                <asp:TextBox runat="server" ID="ConfirmNewPassword" TextMode="Password" />
            </li>
        </ol>
        <asp:Button ID="EditProfile" runat="server" OnClick="Edit_Profile" Text="Edit Profile" />
    </fieldset>
</asp:Content>
