﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using GameCloud.MyClasses;

namespace GameCloud.User
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string usertype = (string)Session["user_type"] ?? "";
            if (usertype.CompareTo("User") != 0)
            {
                Response.Redirect("~/MyAccount/Login");
            }
            Dictionary<string, string> profile = GetProfile();
            if (Email.Text.CompareTo("") == 0 && Age.Text.CompareTo("") == 0)
            {
                Email.Text = profile["mail"];
                Age.Text = profile["age"];
                Cash.Text = Convertor.convert(Convert.ToDouble(profile["cash"]),Session["currency"].ToString());
                CurrencyList.Items.FindByValue(profile["moneytype"]).Selected = true;
            }
        }

        private Dictionary<string, string> GetProfile()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("get_profile", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@username", Session["user_name"]));
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            Dictionary<string, string> profile = new Dictionary<string, string>();
            profile["mail"] = reader["mail"].ToString();
            profile["age"] = reader["age"].ToString();
            profile["cash"] = reader["cash"].ToString();
            profile["moneytype"] = reader["moneytype"].ToString();
            reader.Close();
            conn.Close();
            return profile;
        }

        protected void Edit_Profile(object sender, EventArgs e)
        {
            try
            {
                int age = Convert.ToInt32(Age.Text);
                if (age < 13)
                {
                    ErrorMessage.Text = "You must be at least 13 years old!";
                    return;
                }
            }
            catch (Exception)
            {
                ErrorMessage.Text = "You age is invalid.";
                return;
            }
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            if (Password.Text.CompareTo("") != 0)
            {
                if ((NewPassword.Text.CompareTo(ConfirmNewPassword.Text) == 0) && (NewPassword.Text.CompareTo("") != 0))
                {
                    SqlCommand cmd = new SqlCommand("validate_pwd_reset", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@username", Session["user_name"]));
                    cmd.Parameters.Add(new SqlParameter("@pwd", Password.Text));
                    cmd.Parameters.Add(new SqlParameter("@new_pwd", NewPassword.Text));
                    cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
                    {
                        Direction = ParameterDirection.Output,
                        Size = 5
                    });
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    if (cmd.Parameters["@return"].Value.ToString().CompareTo("True") != 0)
                    {
                        ErrorMessage.Text = "Cannot update the password.";
                        return;
                    }
                }
                else
                {
                    ErrorMessage.Text = "Cannot update the password.";
                    return;
                }
            }
            Dictionary<string, string> profile = GetProfile();
            if ((profile["mail"].CompareTo(Email.Text) != 0) || (profile["age"].CompareTo(Age.Text) != 0) ||
                (profile["moneytype"].CompareTo(CurrencyList.SelectedValue) != 0))
            {
                String email = Email.Text, age = Age.Text, moneytype = CurrencyList.SelectedValue;
                Session["currency"] = moneytype;
                if (Email.Text.CompareTo("") == 0)
                {
                    email = profile["mail"];
                }
                else if (Age.Text.CompareTo("") == 0)
                {
                    age = profile["age"];
                }
                SqlCommand cmd = new SqlCommand("update_profile", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@username", Session["user_name"]));
                cmd.Parameters.Add(new SqlParameter("@mail", email));
                cmd.Parameters.Add(new SqlParameter("@age", age));
                cmd.Parameters.Add(new SqlParameter("@moneytype", moneytype));
                cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
                {
                    Direction = ParameterDirection.Output,
                    Size = 5
                });
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                if (cmd.Parameters["@return"].Value.ToString() != "True")
                {
                    ErrorMessage.Text = "Error on profile update.";
                    return;
                }
                Session["currency"] = moneytype;
                Response.Redirect("~/User/Profile");
            }
            ErrorMessage.Text = "Profile updated with success!";
        }
    }
}