﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using GameCloud.MyClasses;

namespace GameCloud.User
{
    public partial class MyGames : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string user_type = (string)Session["user_type"] ?? "";
            if (user_type.CompareTo("User") != 0)
            {
                Response.Redirect("~/");
            }
            List<MyModels.Game> games = new List<MyModels.Game>();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            /* obter ids */
            SqlCommand cmd = new SqlCommand("get_purchases_from_user", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@user", Session["user_name"]));
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<int> listID = new List<int>();
            while (reader.Read())
            {
                listID.Add(Convert.ToInt32(reader["id"]));
            }
            reader.Close();
            /* obter games */
            foreach (int id in listID)
            {
                cmd = new SqlCommand("get_game", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@ind", id));
                reader = cmd.ExecuteReader();
                reader.Read();
                games.Add(new MyModels.Game(reader["name"].ToString(), Convertor.convert(Convert.ToDouble(reader["price"]),Session["currency"].ToString()), reader["description"].ToString(), id));
                reader.Close();
            }
            conn.Close();
            GameList.DataSource = games;
            GameList.DataBind();
        }
    }
}