﻿
/* Clean Database */

--drop table GameCloud.Purchase;

--drop table GameCloud.Game;

--drop table GameCloud.Users;

--drop table GameCloud.MoneyType;

--drop xml schema collection GameCloud.Jogos;

--drop schema GameCloud;

/*
declare @ind int;
set @ind = 1;
exec get_game @ind;
*/

go

/* SQL Schema */

create schema GameCloud;

go

/* XML Schema */
create xml schema collection GameCloud.Compras as
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns="http://www.compras.com"
targetNamespace="http://www.compras.com"
elementFormDefault="qualified">
  <xs:element name="purchase">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="username" type="xs:string" />
        <xs:element name="idgame" type="xs:int" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>';

create xml schema collection GameCloud.Jogos as
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns="http://www.jogos.com"
targetNamespace="http://www.jogos.com"
elementFormDefault="qualified">
  <xs:element name="game">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="name" type="xs:string" />
        <xs:element name="developer" type="xs:string" />
        <xs:element name="releaseDate" type="xs:date" />
        <xs:element name="genre">
          <xs:simpleType>
            <xs:restriction base="xs:string">
              <xs:enumeration value="Online Battle Arena"/>
              <xs:enumeration value="RPG"/>
              <xs:enumeration value="Platform"/>
              <xs:enumeration value="Adventure"/>
              <xs:enumeration value="Action"/>
            </xs:restriction>
          </xs:simpleType>
        </xs:element>
        <xs:element name="description" type="xs:string" />
        <xs:element name="price" type="xs:float" />
        <xs:element name="wishlist">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs="0" maxOccurs="unbounded" name="username" type="xs:string" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>';

go

/* Database Enum Tables */

--delete from GameCloud.Gender;

-- Currency

create table GameCloud.MoneyType (
	currency varchar(3) primary key		
);

insert into GameCloud.MoneyType values('EUR');
insert into GameCloud.MoneyType values('USD');
insert into GameCloud.MoneyType values('GBP');
insert into GameCloud.MoneyType values('JPY');
insert into GameCloud.MoneyType values('CAD');
insert into GameCloud.MoneyType values('AUD');
insert into GameCloud.MoneyType values('CNY');

--delete from GameCloud.MoneyType;

/* Database Tables */

-- Users

create table GameCloud.Users (
    username varchar(32) primary key not null,	
    passwd varchar(16) not null,
    age int check(age >= 13) not null,
	mail varchar(32) not null,
	cash float check(cash > 0.0) not null,
	moneytype varchar(3) references GameCloud.MoneyType(currency) not null	
);

insert into GameCloud.Users values('hugo','abcd',21,'hugo@ua.pt',150,'EUR');
insert into GameCloud.Users values('simon','aaaa',21,'simon@ua.pt',150,'EUR');
insert into GameCloud.Users values('jessicah','sup',25,'jessicah@ua.pt',150,'EUR');
insert into GameCloud.Users values('admin','zzzz',40,'admin@ua.pt',150,'USD');

--delete from GameCloud.Users;

-- Game

create table GameCloud.Game (
	id int identity(1,1) primary key,
	game xml (GameCloud.Jogos) not null
);

insert into GameCloud.Game values('<game xmlns="http://www.jogos.com"><name>Pokémon Red</name><developer>Game Freak</developer><releaseDate>1996-02-27</releaseDate>
<genre>RPG</genre><description>Pokémon Red Version and Blue Version, originally released in Japan as Pocket Monsters: Red and Green,  are role-playing video games developed by Game Freak and published by Nintendo for the Game Boy. They are the first installments of the Pokémon series. They were first released in Japan in 1996 as Red and Green, with Blue being released later in the year as a special edition. They were later released as Red and Blue in North America, Europe and Australia over the following three years. Pokémon Yellow, a special edition version, was released roughly a year later.</description><price>25</price><wishlist></wishlist></game>');
insert into GameCloud.Game values('<game xmlns="http://www.jogos.com"><name>Super Mario 64</name><developer>Nintendo</developer><releaseDate>1996-06-23</releaseDate>
<genre>Platform</genre><description>Super Mario 64 is a platform game, published by Nintendo and developed by its EAD division, for the Nintendo 64. Along with Pilotwings 64, it was one of the launch titles for the console. It was released in Japan on June 23, 1996, and later in North America, Europe, and Australia. Super Mario 64 has sold over eleven million copies. An enhanced remake called Super Mario 64 DS was released for the Nintendo DS in 2004. As one of the earlier three dimensional (3D) platform games, Super Mario 64 features free-roaming analog degrees of freedom, large open-ended areas, and true 3D polygons as opposed to two-dimensional (2D) sprites. It established a new archetype for the genre, much as Super Mario Bros. did for 2D sidescrolling platformers. Hailed as revolutionary, the game left a lasting impression on 3D game design, particularly notable for its use of a dynamic camera system and the implementation of its analog control.</description><price>20</price><wishlist></wishlist></game>');
insert into GameCloud.Game values('<game xmlns="http://www.jogos.com"><name>League of Legends</name><developer>Riot Games</developer><releaseDate>2010-10-27</releaseDate>
<genre>Online Battle Arena</genre><description>League of Legends (LoL) is a multiplayer online battle arena video game developed and published by Riot Games for Microsoft Windows and Mac OS X, inspired by the mod Defense of the Ancients for the video game Warcraft III: The Frozen Throne. It is a free-to-play game, supported by micro-transactions. It was first announced on October 7, 2008, and released on October 27, 2009. League of Legends was generally well received at release, and has grown in popularity in the years since. According to a 2012 Forbes article, League of Legends is the most played PC game in the world in terms of the number of hours played.</description><price>20</price><wishlist><username>hugo</username><username>simon</username><username>jessicah</username></wishlist></game>');
insert into GameCloud.Game values('<game xmlns="http://www.jogos.com"><name>Amnesia the Dark Descent</name><developer>Frictional Games</developer><releaseDate>2009-09-08</releaseDate>
<genre>Action</genre><description>Sonic Generations is a platform video game developed by Sonic Team and published by Sega for the PlayStation 3, Xbox 360, Microsoft Windows and Nintendo 3DS platforms. Released in 2011, it celebrates the 20th anniversary of the original Sonic the Hedgehog game s release. On July 2, 2012, the game was made available for digital download on PlayStation Network, and was made available on the Games on Demand service on October 16, 2012. It is also available on the Nintendo eShop. In Japan, the two versions are given subtitles: the Xbox 360/PS3 version of the game is given Shiro no Jikū White Spacetime) while the 3DS version is given Ao no Bōken.</description><price>15</price><wishlist><username>hugo</username><username>simon</username></wishlist></game>');

--delete from GameCloud.Game;

-- Purchase

create table GameCloud.Purchase (
	id int identity(1,1) primary key,
	purchase xml(GameCloud.Compras) not null
);

insert into GameCloud.Purchase values('<purchase xmlns="http://www.compras.com"><username>hugo</username><idgame>1</idgame></purchase>');
insert into GameCloud.Purchase values('<purchase xmlns="http://www.compras.com"><username>hugo</username><idgame>2</idgame></purchase>');
insert into GameCloud.Purchase values('<purchase xmlns="http://www.compras.com"><username>simon</username><idgame>1</idgame></purchase>');
insert into GameCloud.Purchase values('<purchase xmlns="http://www.compras.com"><username>simon</username><idgame>2</idgame></purchase>');
insert into GameCloud.Purchase values('<purchase xmlns="http://www.compras.com"><username>jessicah</username><idgame>1</idgame></purchase>');

--delete from GameCloud.Purchase;

/* Stored Procedures */

--get user currency
go
if object_id('get_user_currency') is not null
        drop proc get_user_currency

go
create proc get_user_currency @username varchar(32) as
begin
	select moneytype from GameCloud.Users where username = @username;
end

--buy a game/offer to another user
go
if object_id('buy_game') is not null
        drop proc buy_game

go
create proc buy_game @idGame int, @UserOffering varchar(32), @UserOffered varchar(32), @return varchar(5) output as
begin
		declare @moneyUser float;
		declare @priceGame float;

		-- get price from game
		with xmlnamespaces('http://www.jogos.com' as gns)
		select  @priceGame = game.value('(/gns:game/gns:price)[1]','float') 
			from GameCloud.Game where id =@idGame;
		
		-- get money user has
		select @moneyUser = cash from GameCloud.Users where username = @UserOffering;

		if( @moneyUser > @priceGame)
		begin
			--money -= price
			declare @newMoney int;
			set @newMoney = @moneyUser - @priceGame;

			update GameCloud.Users
			set cash = @newMoney
			where username = @UserOffering;

			--add to purchases
			declare @xml xml;
			set @xml = '<purchase xmlns="http://www.compras.com"><username>'+@UserOffered+'</username><idgame>'+cast(@idGame as varchar(5))+'</idgame></purchase>'
			insert into GameCloud.Purchase values(@xml);

			--check wish list: if <@idUserOffered> is on wishlist of <@idGame>, remove it
			update GameCloud.Game 
			set game.modify('declare namespace gns="http://www.jogos.com";
			delete (/gns:game/gns:wishlist/gns:username[. = sql:variable("@UserOffered") ] ) ')
			where id = @idGame;
			set @return ='True';
		end
		else
			set @return = 'False';
end

--get purchases from user
go
if object_id('get_purchases_from_user') is not null
        drop proc get_purchases_from_user

go
create proc get_purchases_from_user @user  varchar(32) as
begin
with xmlnamespaces('http://www.compras.com' as pch)
	select purchase.query('data(/pch:purchase/pch:idgame)') as id 
	from GameCloud.Purchase 
	where purchase.exist('/pch:purchase/pch:username[. = sql:variable("@user")]') =1;
end

--get existing games
go
if object_id('get_existing_games') is not null
        drop proc get_existing_games

go
create proc get_existing_games as
begin
with xmlnamespaces('http://www.jogos.com' as gns)
	select game.query('data(/gns:game/gns:name)') as game 
	from GameCloud.Game;
end
--add game to wish list
go
if object_id('add_user_to_wish_list') is not null
        drop proc add_user_to_wish_list

go
create proc add_user_to_wish_list @idGame int, @idUser varchar(32) as
begin
	update GameCloud.Game 
	set game.modify('declare namespace gns="http://www.jogos.com";
	insert (<gns:username>sql_variable("@idUser")</gns:username>) as last into (/gns:game[1]/gns:wishlist[1])')
	where id = @idGame; 		
end

go
if object_id('get_games_wish_list') is not null
        drop proc get_games_wish_list

go
create proc get_games_wish_list @idGame int as
begin
	with xmlnamespaces('http://www.jogos.com' as gns)
	select  game.query('(for $user in /gns:game/gns:wishlist/gns:username return string($user))') as usernames	from GameCloud.Game where id = @idGame;		
end

--get ids from games filtered by genre
go
if object_id('get_games_by_genre') is not null
        drop proc get_games_by_genre

go
create proc get_games_by_genre @genre varchar(32) as
begin

	declare @i int = 0;
	declare @rows int;
	select @rows = count(*) from GameCloud.Game;
	
	--temporary table
	create table #tempTable(id int);

	while( @i <=  @rows)
	begin
		declare @gen varchar(32);

		--get genre from game[i]
		if( exists( select * 
					from GameCloud.Game 
					where id = @i and 
							game.exist('declare namespace  gns="http://www.jogos.com"; (/gns:game[1]/gns:genre[. = sql:variable("@genre")] ) ') = 1) )
			insert into #tempTable values(@i);

		set @i = @i + 1;
	end

	select id from #tempTable;

end
--get game[@index]

go
if object_id('get_game') is not null
        drop proc get_game
		
go
create proc get_game @ind int as
begin
	with xmlnamespaces('http://www.jogos.com' as gns)
	select game.value('(/gns:game/gns:price)[1]','float') as price,
	game.value('(/gns:game/gns:name)[1]','varchar(32)') as name,
	 game.value('(/gns:game/gns:developer)[1]','varchar(32)') as developer,
	game.value('(/gns:game/gns:genre)[1]','varchar(32)') as genre,
	game.value('(/gns:game/gns:description)[1]','nvarchar(max)') as [description],
	game.value('(/gns:game/gns:releaseDate)[1]','date') as releaseDate
	from GameCloud.Game where id = @ind;
end

go
if object_id('get_id_games') is not null
    drop procedure get_id_games

go
create procedure get_id_games as
begin
	select id from GameCloud.Game;
end

--add user
go
if object_id('validate_registry') is not null
        drop proc validate_registry
go
create procedure validate_registry @username varchar(16), @pwd varchar(16), @age int, @email varchar(32), @return varchar(5) output as
begin
	if(exists(select * from GameCloud.Users where username = @username))
		set @return = 'False';	
	else
		begin	
			insert into GameCloud.Users values (@username, @pwd, @age,@email, 10.0, 'EUR');
			set @return = 'True';
		end
end

--update user's profile
go
if object_id('update_profile') is not null
        drop proc update_profile

go
create proc update_profile @username varchar(16), @age int, @mail varchar(32), @moneyType varchar(3), @return varchar(5) output as
begin
        if(exists(        select *	
                                from GameCloud.Users	
                                where username = @username ))	
                begin	
                        update GameCloud.Users set age = @age, mail = @mail, moneyType=@moneyType where username = @username	
                        if (@@rowcount = 0) set @return = 'False';
						else set @return = 'True';
                end	
        else	
                set @return = 'False'
end

--change password
go
if object_id('validate_pwd_reset') is not null
        drop proc validate_pwd_reset

go
create proc validate_pwd_reset @username varchar(16), @pwd varchar(16), @new_pwd varchar(16), @return varchar(5) output as
begin

        update GameCloud.Users set passwd = @new_pwd where username = @username and passwd = @pwd;

        if (@@rowcount = 0)	
			set @return = 'False'	
        else	
            set @return = 'True'
end
--charge money
go
if object_id('charge_money') is not null
        drop proc charge_money

go
create proc charge_money @user varchar(16), @money float, @return varchar(5) output as
begin
	update GameCloud.Users set cash += @money where username = @user;
	if (@@rowcount = 0)	
			set @return = 'False'	
        else	
            set @return = 'True'
end

--validate login
go
if object_id('validate_login') is not null
	drop procedure validate_login

go
create procedure validate_login @username varchar(16), @pwd varchar(16), @return varchar(5) output as
begin
	if(exists(select * from GameCloud.Users where username = @username and passwd = @pwd))
		set @return = 'True'
	else
		set @return = 'False'
end

--Admin: add game

go
if object_id('add_game') is not null
        drop proc add_game

go
create proc add_game @name varchar(32), @developer varchar(32),
								@genre varchar(32), @price varchar(32), @releaseDate date, 
								@description varchar(max), @return varchar(5) output as
begin
	declare @xml xml;
	set @xml = '<game xmlns="http://www.jogos.com"><name>'+@name+'</name><developer>'+@developer+'</developer><releaseDate>'+CAST(@releaseDate AS VARCHAR(50))+'</releaseDate>
<genre>'+@genre+'</genre><description>'+@description+'</description><price>'+@price+'</price><wishlist></wishlist></game>';
	insert into GameCloud.Game values(@xml);
	if (@@rowcount = 0)	
			set @return = 'False'	
       else	
            set @return = 'True'
end

--check if user is admin
go
if object_id('check_user_is_admin') is not null	
        drop proc check_user_is_admin
		
go
create proc check_user_is_admin @user varchar(16), @return varchar(5) output as
begin
	if(@user = 'admin')
	begin
		if(exists(select * from GameCloud.Users where username = @user))	
			set @return = 'True';
		else
			set @return = 'False';
	end
end

--get user info
go
if object_id('get_profile') is not null	
        drop proc get_profile

go
create proc get_profile @username varchar(16) as
begin
        select age, mail, cash, moneytype	
        from GameCloud.Users	
        where username = @username;
end