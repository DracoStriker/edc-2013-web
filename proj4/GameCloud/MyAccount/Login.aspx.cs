﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace GameCloud.MyAccount
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string user_type = (string)Session["user_type"] ?? "";
            if (user_type.CompareTo("") != 0)
            {
                Response.Redirect("~/");
            }
        }

        protected void Check_Login(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("validate_login", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@username", UserName.Text));
            cmd.Parameters.Add(new SqlParameter("@pwd", Password.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                cmd = new SqlCommand("get_user_currency", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@username", UserName.Text));
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                Session["currency"] = reader["moneytype"];
                reader.Close();
                
                cmd = new SqlCommand("check_user_is_admin", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@user", UserName.Text));
                cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
                {
                    Direction = ParameterDirection.Output,
                    Size = 5
                });
                cmd.ExecuteNonQuery();
                conn.Close();
                Session["user_name"] = UserName.Text;
                Session.Timeout = 1;
                if (cmd.Parameters["@return"].Value.ToString() == "True")
                {
                    Session["user_type"] = "Admin";
                }
                else
                {
                    Session["user_type"] = "User";
                }
                Response.Redirect("~/");
            }
            else
            {
                ErrorMessage.Text = "Invalid Username or Password.";
                conn.Close();
            }
        }
    }
}