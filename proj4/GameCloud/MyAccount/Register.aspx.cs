﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace GameCloud.MyAccount
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string user_type = (string)Session["user_type"] ?? "";
            if (user_type.CompareTo("") != 0)
            {
                Response.Redirect("~/");
            }
        }

        protected void Check_Register(object sender, EventArgs e)
        {
            try
            {
                int age = Convert.ToInt32(Age.Text);
                if (age < 13)
                {
                    ErrorMessage.Text = "You must be at least 13 years old!";
                    return;
                }
            }
            catch (Exception)
            {
                ErrorMessage.Text = "You age is invalid.";
                return;
            }
            if (Password.Text.CompareTo(ConfirmPassword.Text) != 0)
            {
                ErrorMessage.Text = "Password don't match.";
                return;
            }
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("validate_registry", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@username", UserName.Text));
            cmd.Parameters.Add(new SqlParameter("@pwd", Password.Text));
            cmd.Parameters.Add(new SqlParameter("@age", Age.Text));
            cmd.Parameters.Add(new SqlParameter("@email", Email.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString().CompareTo("True") == 0)
            {
                Response.Redirect("~/MyAccount/Login.aspx");
            }
            else
            {
                ErrorMessage.Text = "Username already exists.";
            }
        }
    }
}