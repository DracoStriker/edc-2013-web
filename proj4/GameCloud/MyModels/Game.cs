﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameCloud.MyModels
{
    public class Game
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public string Description { get; set; }
        public string Genre { get; set; }
        public string Date { get; set; }
        public string Developer { get; set; }
        public int ID { get; set; }

        public Game(string name, string price, string description, int ID)
        {
            this.Name = name;
            this.Price = price;
            this.Description = description;
            this.ID = ID;
        }

        public Game(string name, string price, string description, string genre, string date, string developer, int ID)
        {
            this.Name = name;
            this.Price = price;
            this.Description = description;
            this.Genre = genre;
            this.Date = date;
            this.Developer = developer;
            this.ID = ID;
        }
    }
}