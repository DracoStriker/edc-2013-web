﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameCloud.MyClasses
{
    public class Convertor
    {
        public static string convert(double euro, string currency)
        {
            net.webservicex.www.CurrencyConvertor convertor = new net.webservicex.www.CurrencyConvertor();
            switch (currency)
            {
                case "AUD":
                    return euro * convertor.ConversionRate(net.webservicex.www.Currency.EUR, net.webservicex.www.Currency.AUD) + " AUD";
                case "CAD":
                    return euro * convertor.ConversionRate(net.webservicex.www.Currency.EUR, net.webservicex.www.Currency.CAD) + " CAD";
                case "CNY":
                    return euro * convertor.ConversionRate(net.webservicex.www.Currency.EUR, net.webservicex.www.Currency.CAD) + " CAD";
                case "GBP":
                    return euro * convertor.ConversionRate(net.webservicex.www.Currency.EUR, net.webservicex.www.Currency.GBP) + " GBP";
                case "JPY":
                    return euro * convertor.ConversionRate(net.webservicex.www.Currency.EUR, net.webservicex.www.Currency.JPY) + " JPY";
                case "USD":
                    return euro * convertor.ConversionRate(net.webservicex.www.Currency.EUR, net.webservicex.www.Currency.USD) + " USD";
                default:
                    return euro + " EUR";
            }
        }
    }
}