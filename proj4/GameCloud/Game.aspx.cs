﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using GameCloud.MyClasses;

namespace GameCloud
{
    public partial class Game : System.Web.UI.Page
    {
        private List<MyModels.Game> games;
        private int id;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.QueryString["game"] ?? "").CompareTo("") == 0)
            {
                Response.Redirect("~/Games");
            }
            id = Convert.ToInt32(Request.QueryString["game"]);
            games = new List<MyModels.Game>();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("get_game", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@ind", id));
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            string user_type = (string)Session["user_type"] ?? "";
            if (user_type.CompareTo("") != 0)
            {
                games.Add(new MyModels.Game(reader["name"].ToString(), Convertor.convert(Convert.ToDouble(reader["price"]),Session["currency"].ToString()), reader["description"].ToString(), reader["genre"].ToString(), reader["releaseDate"].ToString(), reader["developer"].ToString(), id));
            }
            else
            {
                games.Add(new MyModels.Game(reader["name"].ToString(), reader["price"].ToString() + " EUR", reader["description"].ToString(), reader["genre"].ToString(), reader["releaseDate"].ToString(), reader["developer"].ToString(), id));
            }
            reader.Close();
            conn.Close();
            movie.DataSource = games;
            movie.DataBind();
            if (user_type.CompareTo("User") == 0)
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
                /* obter ids */
                cmd = new SqlCommand("get_purchases_from_user", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.Add(new SqlParameter("@user", Session["user_name"]));
                conn.Open();
                reader = cmd.ExecuteReader();
                List<int> listID = new List<int>();
                while (reader.Read())
                {
                    listID.Add(Convert.ToInt32(reader["id"]));
                }
                reader.Close();
                conn.Close();
                if (!listID.Contains(id))
                {
                    Purchase_Button.Visible = true;
                }
                //Purchase_Button.Visible = true;
                //OfferLabel.Visible = true;
                //OfferUsername.Visible = true;
                //Gift_Button.Visible = true;
            }
        }

        protected void Purchase_Product(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            /* obter ids */
            SqlCommand cmd = new SqlCommand("get_purchases_from_user", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@user", Session["user_name"]));
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<int> listID = new List<int>();
            while (reader.Read())
            {
                listID.Add(Convert.ToInt32(reader["id"]));
            }
            reader.Close();
            conn.Close();
            if (listID.Contains(id))
            {
                ErrorMessage.Text = "You already have the game.";
                return;
            }
            /* comprar */
            cmd = new SqlCommand("buy_game", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@idGame", id));
            cmd.Parameters.Add(new SqlParameter("@UserOffering", Session["user_name"]));
            cmd.Parameters.Add(new SqlParameter("@UserOffered", Session["user_name"]));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                ErrorMessage.Text = "Item purchased with success!";
            }
            else
            {
                ErrorMessage.Text = "You already purchased this item or you don't have enough money.";
            }
        }

        protected void Offer_Game(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("buy_game", conn)
            {
                CommandType = CommandType.StoredProcedure
            };
            cmd.Parameters.Add(new SqlParameter("@idGame", id));
            cmd.Parameters.Add(new SqlParameter("@UserOffering", Session["user_name"]));
            cmd.Parameters.Add(new SqlParameter("@UserOffered", OfferUsername.Text));
            cmd.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            if (cmd.Parameters["@return"].Value.ToString() == "True")
            {
                GiftErrorMessage.Text = "Item purchased with success!";
            }
            else
            {
                GiftErrorMessage.Text = "User already has the game or you don't have enough money.";
            }
        }
    }
}